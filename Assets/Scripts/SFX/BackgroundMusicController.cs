﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SFX
{
    public class BackgroundMusicController : MonoBehaviour
    {
        [SerializeField] private List<BackgroundMusicClip> m_Clips;
        [SerializeField] private AudioSource m_Source;
        
        private int CurrentIndex;

        public void StartPlay()
        {
            m_Source.Stop();
            CurrentIndex = 0;
            m_Source.clip = m_Clips.Find(clipToFind => clipToFind.Order == CurrentIndex).Clip;
            m_Source.Play();
        }

        public void StartPlay(int fromIndex)
        {
            m_Source.Stop();
            CurrentIndex = fromIndex;
            m_Source.clip = m_Clips.Find(clipToFind => clipToFind.Order == CurrentIndex).Clip;
            m_Source.Play();
        }

        private void Update()
        {
            if (!m_Source.isPlaying)
            {
                CurrentIndex++;
                StartPlay(CurrentIndex);
            }
        }
    }

    [Serializable]
    internal class BackgroundMusicClip
    {
        public AudioClip Clip;
        public int Order;
    }

}
