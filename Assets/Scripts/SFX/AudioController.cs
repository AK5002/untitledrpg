﻿using UnityEngine;
using UnityEngine.Audio;

namespace SFX
{
    public class AudioController : MonoBehaviour
    {
        [SerializeField] private AudioMixer m_Mixer;

        public void SetVolume(AudioType group, float volume)
        {
            string paramName;

            switch (group)
            {
                case AudioType.Effects:
                    paramName = "EffectsVolume";
                    break;
                case AudioType.Music:
                    paramName = "MusicVolume";
                    break;
                default:
                    paramName = "MasterVolume";
                    break;
            }

            Debug.Log(paramName);
            m_Mixer.SetFloat(paramName, volume);
        }
        
        public enum AudioType
        {
            Master,
            Effects,
            Music,
        }
    }
}