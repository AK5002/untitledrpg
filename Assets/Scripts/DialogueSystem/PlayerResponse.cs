using UnityEngine;

namespace DialogueSystem
{
    public class PlayerResponse : MonoBehaviour
    {
        [SerializeField] private ResponseManner m_Manner;
        [SerializeField] private ResponseType m_Type;
        [SerializeField] private int m_ToOrderID;
        [SerializeField] private string m_Value;

        public ResponseManner Manner => m_Manner;
        public ResponseType Type => m_Type;
        public int ToOrderID => m_ToOrderID;
        public string Value => m_Value;
    }
}