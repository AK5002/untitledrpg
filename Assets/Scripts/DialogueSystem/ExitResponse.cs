using UnityEngine;

namespace DialogueSystem
{
    public class ExitResponse : ResponseType, IExitResponse
    {
        [SerializeField] private ExitType m_Type;

        public ExitType Type => m_Type;
        public override string Hint => " (Exit Dialogue)";
    }
    
    public interface IExitResponse
    { 
        ExitType Type { get; }
    }
    
}