using System.Collections.Generic;
using UnityEngine;

namespace DialogueSystem
{
    public enum ResponseManner
    {
        Rude = -1,
        Neutral = 0,
        Generous = 1,
    }

    public abstract class ResponseType : MonoBehaviour
    {
        public abstract string Hint { get; }
    }
    
   

    public enum ExitType
    {
        Normal,
        Trade,
        Combat,
    }
}