﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using Core.Game;
using Core.Game.Items;
using Core.Game.NPC;
using Core.Game.Relations;
using QuestSystem;
using UnityEngine;

namespace DialogueSystem
{
    public class DialogueController : MonoBehaviour
    {
        private Dictionary<int, HashSet<NodeID>> UsedNodes = new Dictionary<int, HashSet<NodeID>>();
        private DialogueTree CurrentTree;
        private List<ResponseType> CurrentPlayerResponseTypes = new List<ResponseType>();
        private List<ResponseType> CurrentNPCResponseTypes = new List<ResponseType>();

        public event Action<int,Factions> OnChangeReputation;
        public event Action<ExitType,IDialogueOwner> OnEndDialogue;
        public event Action<List<Item>, Inventory,bool, QuestID, bool> OnTrade;
        public event Action<Quest> OnQuest;
        public event Action<string,ResponseType,List<DialogueMenuResponse>> OnNodeChanged;
        public event Func<List<Item>, bool> OnTradeAllowed;
        public event Func<QuestID,bool> OnIfQuestAllows;
        public void BeginDialogue(DialogueTree tree)
        {
            if (CurrentTree != null || tree == null) return;
            
            tree.transform.SetParent(transform);
            CurrentTree = tree;
            if(!UsedNodes.ContainsKey(CurrentTree.ID))
                UsedNodes.Add(CurrentTree.ID, new HashSet<NodeID>());
            RequestUIUpdate();
        }

        public void OnDirectionChosenHandler(int orderID)
        {
            if (CurrentTree == null) return;
            
            var response = CurrentTree.CurrentNode.Responses.Find(toFind => toFind.ToOrderID== orderID);

            if (response.Manner != ResponseManner.Neutral)
            {
                OnChangeReputation?.Invoke((int)response.Manner,CurrentTree.Owner.Faction);
            }
            if (response.Type is IExitResponse exitResponse)
            {
                OnEndDialogue?.Invoke(exitResponse.Type, CurrentTree.Owner);
                ProcessResponses();
                EndDialogue();
            }
            else
            {
                if(response.Type != null) CurrentPlayerResponseTypes.Add(response.Type);
                
                if(orderID == -1 && !(response.Type is IExitResponse))
                    CurrentTree.ReturnToPreviousNode();
                else CurrentTree.ProgressTo(orderID);
                
                if(!CurrentTree.CurrentNode.ID.ReUsable && !UsedNodes[CurrentTree.ID].Contains(CurrentTree.CurrentNode.ID))
                    UsedNodes[CurrentTree.ID].Add(CurrentTree.CurrentNode.ID);
                if (CurrentTree.CurrentNode.Type != null)
                {
                    CurrentNPCResponseTypes.Add(CurrentTree.CurrentNode.Type );
                }
                RequestUIUpdate(); 
            }
        }

        private void EndDialogue()
        {
            if(CurrentTree != null) Destroy(CurrentTree);
            CurrentTree = null;
            CurrentPlayerResponseTypes = new List<ResponseType>();
            CurrentNPCResponseTypes = new List<ResponseType>();
        }

        private void RequestUIUpdate()
        {
            List<DialogueMenuResponse> responses = new List<DialogueMenuResponse>();
            bool questDependentPassed = false;
            foreach (var playerResponse in CurrentTree.CurrentNode.Responses)
            {
                if(playerResponse.ToOrderID == -1)  responses.Add(new DialogueMenuResponse(playerResponse.Value, playerResponse.ToOrderID,playerResponse.Manner,playerResponse.Type));
                else
                {
                    var questDependentID = CurrentTree.GetNextMode(playerResponse.ToOrderID).DependentQuestID;
                    if (!UsedNodes[CurrentTree.ID].Contains(CurrentTree.GetNextMode(playerResponse.ToOrderID).ID))
                    {
                        if (questDependentID != QuestID.None &&
                            OnIfQuestAllows(CurrentTree.GetNextMode(playerResponse.ToOrderID).DependentQuestID) &&
                            !questDependentPassed)
                        {
                            responses.Add(new DialogueMenuResponse(playerResponse.Value, playerResponse.ToOrderID,playerResponse.Manner,playerResponse.Type));
                            questDependentPassed = true;
                        }

                        if (questDependentID == QuestID.None)
                        {
                            if (playerResponse.Type is ITradeResponse tradeResponse)
                            {
                                if (OnTradeAllowed(tradeResponse.Items))
                                    responses.Add(new DialogueMenuResponse(playerResponse.Value, playerResponse.ToOrderID,
                                        playerResponse.Manner, playerResponse.Type));
                            }
                            else responses.Add(new DialogueMenuResponse(playerResponse.Value, playerResponse.ToOrderID,playerResponse.Manner,playerResponse.Type));
                        }
                    }
                }
               
            }
            OnNodeChanged?.Invoke(CurrentTree.CurrentNode.NonPlayableResponse, CurrentTree.CurrentNode.Type,responses);
        }
        
        private void ProcessResponses()
        {
            foreach (var responseType in CurrentPlayerResponseTypes.Concat(CurrentNPCResponseTypes))
            {
                if (responseType is ITradeResponse tradeResponse)
                {
                    OnTrade?.Invoke(tradeResponse.Items, CurrentTree.Inventory,CurrentNPCResponseTypes.Contains(responseType),tradeResponse.QuestID,tradeResponse.IsQuestReward);
                }
                if (responseType is IQuestResponse questResponse)
                {
                    var quest = Instantiate(questResponse.Quest);
                    quest.DialogueNodeInitialize(CurrentTree.Owner);
                    OnQuest?.Invoke(quest);
                }
            }
        }
        public void AbortDialogue()
        {
            if(CurrentTree != null) Destroy(CurrentTree.gameObject);
            CurrentTree = null;
            CurrentPlayerResponseTypes = new List<ResponseType>();
            CurrentNPCResponseTypes = new List<ResponseType>();
        }
    }

    public struct DialogueMenuResponse
    {
        public string Value { get; }
        public int OrderID { get; }
        public ResponseManner Manner { get; }
        public ResponseType Type { get; }

        public DialogueMenuResponse(string value, int orderId, ResponseManner manner, ResponseType type)
        {
            Value = value;
            OrderID = orderId;
            Manner = manner;
            Type = type;
        }
    }
}
