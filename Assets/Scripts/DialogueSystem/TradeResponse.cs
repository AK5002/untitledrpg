using System.Collections.Generic;
using Core.Game.Items;
using QuestSystem;
using UnityEngine;

namespace DialogueSystem
{
    public class TradeResponse : ResponseType, ITradeResponse
    {
        [SerializeField] private List<Item> m_Items;
        [SerializeField] private QuestID m_QuestID = new QuestID(-1, -1);
        [SerializeField] private bool m_IsQuestReward;

        public List<Item> Items => m_Items;
        public QuestID QuestID => m_QuestID;
        public bool IsQuestReward => m_IsQuestReward;

        public override string Hint
        {
            get
            {
                string retVal = "";

                foreach (var item in m_Items)
                {
                    retVal += $" {item.Name},";
                }
                return retVal;
            }
        }
}

    public interface ITradeResponse
    {
        List<Item> Items { get; }
        QuestID QuestID { get; }
        bool IsQuestReward { get; }
    }
}