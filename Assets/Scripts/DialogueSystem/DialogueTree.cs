﻿using Core.Game;
using Core.Game.NPC;
using UnityEngine;

namespace DialogueSystem
{
    public class DialogueTree : MonoBehaviour
    {
        [SerializeField] private int m_ID;
        [SerializeField] private DialogueNode m_CurrentNode;
        [SerializeField] private DialogueNode m_RootNode;

        public int ID => m_ID;
        public IDialogueOwner Owner { get; private set; }
        public Inventory Inventory { get; private set; }
        public DialogueNode CurrentNode => m_CurrentNode;
        private DialogueNode PreviousNode;

        public void Setup(IDialogueOwner owner, Inventory inventory)
        {
            m_CurrentNode = m_RootNode;
            Debug.Log(owner);
            Owner = owner;
            Inventory = inventory;
        }

        public void ProgressTo(int orderID)
        {
            if(orderID>= CurrentNode.ChildNodes.Count) return;

            PreviousNode = m_CurrentNode;
            m_CurrentNode = m_CurrentNode.ChildNodes[orderID];
        }

        public void ChangeTo(int orderID)
        {
            
        }

        public void ReturnToPreviousNode()
        {
            if (PreviousNode == null) return;
            
            m_CurrentNode = PreviousNode;
        }

        public DialogueNode GetNextMode(int orderID)
        {
            if(orderID>= CurrentNode.ChildNodes.Count || orderID == -1) return null;
            
           return m_CurrentNode.ChildNodes[orderID];
        }
    }
}

