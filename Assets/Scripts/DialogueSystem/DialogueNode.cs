using System;
using System.Collections.Generic;
using QuestSystem;
using UnityEngine;

namespace DialogueSystem
{
    public class DialogueNode : MonoBehaviour
    {
        [SerializeField] private NodeID m_ID;
        [SerializeField] private string m_Response;
        [SerializeField] private QuestID m_DependentQuestID = QuestID.None;
        [SerializeField] private List<DialogueNode> m_ChildNodes;
        [SerializeField] private List<PlayerResponse> m_PlayerResponses;
        public string NonPlayableResponse => m_Response;
        public List<DialogueNode> ChildNodes => m_ChildNodes;
        public List<PlayerResponse> Responses => m_PlayerResponses;
        public ResponseType Type;
        public NodeID ID => m_ID;
        public QuestID DependentQuestID => m_DependentQuestID;
    }

    [Serializable]
    public struct NodeID
    {
        public int Depth;
        public int Order;
        public bool ReUsable;
    }
}