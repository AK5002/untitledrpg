using QuestSystem;
using UnityEngine;

namespace DialogueSystem
{
    public class QuestResponse : ResponseType,IQuestResponse
    {
        [SerializeField] private Quest m_Quest;

        public Quest Quest => m_Quest;
        public override string Hint => $" Receive Quest: {m_Quest.QuestData.Name}";
    }

    public interface IQuestResponse
    {
        Quest Quest { get; }
    }
}