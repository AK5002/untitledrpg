using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace InGameNotificationSystem
{
    public class NotificationController:MonoBehaviour
    {
        [SerializeField] private Notification m_NotificationPrefab;
        [SerializeField] private float m_Duration;
        
        private Queue<Notification> CurrentNotifications = new Queue<Notification>();

        public void AddNotification(string text)
        {
            var notification = Instantiate(m_NotificationPrefab, transform);
            MoveCurrentNotifications();
            notification.Setup(text,m_Duration,OnNotificationDestroyedHandler);
            CurrentNotifications.Enqueue(notification);
        }
        
        private void MoveCurrentNotifications()
        {
            foreach (var notification in CurrentNotifications)
            {
                notification.RectTransform.DOAnchorPos(notification.RectTransform.anchoredPosition +new Vector2(0,-250),1f);
            }
        }

        private void OnNotificationDestroyedHandler(Notification notification)
        {
            if(CurrentNotifications.Peek() != notification) return;
            CurrentNotifications.Dequeue();
        }
    }
}