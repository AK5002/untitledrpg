using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace InGameNotificationSystem
{
    public class Notification : MonoBehaviour
    {
        [SerializeField] private RectTransform m_RectTransform;
        [SerializeField] private CanvasGroup m_CanvasGroup;
        [SerializeField] private Text m_Text;

        public RectTransform RectTransform => m_RectTransform;
        private float Duration;

        public void Setup(string text, float duration, Action<Notification> onNotificationDestroyed)
        {
            m_Text.text = text;
            Duration = duration;
            m_CanvasGroup.alpha = 0;
            Sequence notificationSequence = DOTween.Sequence();
            notificationSequence.Append(m_CanvasGroup.DOFade(1, duration/2).SetEase(Ease.Linear))
                .Append(m_CanvasGroup.DOFade(0, duration/2).SetEase(Ease.Linear).SetDelay(2f)).OnComplete(() =>
                {
                    Destroy(gameObject);
                    onNotificationDestroyed(this);
                });
        }
    }
}