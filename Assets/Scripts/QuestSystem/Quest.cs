using System;
using System.Collections.Generic;
using Core.Game.NPC;
using UnityEngine;

namespace QuestSystem
{
    [CreateAssetMenu(fileName = "quest", menuName = "ScriptableObjects/Quests/GenericQuest")]
    public class Quest : ScriptableObject
    {
        [SerializeField] private QuestID m_ID;
        [SerializeField] private QuestClass m_Class;
        [SerializeField] private QuestData m_QuestData;
        
        public QuestID ID => m_ID;
        public QuestClass Class => m_Class;
        public IDialogueOwner Giver { get; private set; }
        public QuestData QuestData => m_QuestData;

        public void DialogueNodeInitialize(IDialogueOwner giver)
        {
            Giver = giver;
        }

        public void UnlockReward()
        {
            Debug.Log(Giver);
            m_QuestData.Description = $"Return to {Giver.CharacterName} and receive reward";
            m_QuestData.IsRewardAvailable = true;
        }
    }
    
    public enum QuestClass
    {
        Main,
        Side
    }

    [Serializable]
    public struct QuestID
    {
        public int ProgressionLevel;
        public int SideQuestNumber;
        
        public static QuestID None => new QuestID(-1,-1);
        public static bool operator ==(QuestID id1, QuestID id2) => id1.ProgressionLevel == id2.ProgressionLevel &&
                                                                    id1.SideQuestNumber == id2.SideQuestNumber;

        public static bool operator !=(QuestID id1, QuestID id2) => !(id1 == id2);

        public QuestID(int progressionLevel, int sideQuestNumber)
        {
            ProgressionLevel = progressionLevel;
            SideQuestNumber = sideQuestNumber;
        }
    }

    [Serializable]
    public struct QuestData
    {
        public string Name;
        public string Description;
        public bool IsRewardAvailable;
        
        public static QuestData None = new QuestData();
    }
}