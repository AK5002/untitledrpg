using Core.Game.Items;
using UnityEngine;

namespace QuestSystem
{
    [CreateAssetMenu(fileName = "obtainquest", menuName = "ObtainItemQuest", order = 0)]
    public class ObtainItemQuest : Quest, IObtainItemQuest
    {
        [SerializeField] private Item m_TargetItem;

        public Item TargetItem => m_TargetItem;
    }

    public interface IObtainItemQuest
    {
        Item TargetItem { get; }
        QuestID ID { get; }
    }
}