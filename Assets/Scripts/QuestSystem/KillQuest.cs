using UnityEngine;

namespace QuestSystem
{
    [CreateAssetMenu(fileName = "KillQuest", menuName = "ScriptableObjects/Quests/KillQuest", order = 0)]
    public class KillQuest : Quest, IKillQuest
    {
        [SerializeField] private string m_TargetName;

        public string TargetName => m_TargetName;
    }

    public interface IKillQuest
    {
        string TargetName { get; }
    }
}