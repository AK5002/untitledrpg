using System;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using Core.Game.Items;
using Services.DependencyInjection;
using UI;
using UnityEngine;
using Object = UnityEngine.Object;

namespace QuestSystem
{
    public class QuestController : MonoBehaviour, IInjectable
    {
        private Quest CurrentMainQuest { get; set; }
        private List<Quest> CurrentSideQuests { get; set; } = new List<Quest>();

        public QuestData MainQuestData
        {
            get
            {
                return CurrentMainQuest == null ? QuestData.None : CurrentMainQuest.QuestData;
            }
        }
        public List<QuestData> CurrentSideQuestData {
            get
            {
                return CurrentSideQuests.Select(quest => quest.QuestData).ToList();
            }
        }
       

        public event Action<IObtainItemQuest> OnObtainItemQuestReceived; 

        public void AddQuest(Quest quest)
        {
            if (quest.Class == QuestClass.Side)
            {
                if (CurrentSideQuests.Count == 20) return;
                CurrentSideQuests.Add(quest);
            }
            else
            {
                if (CurrentMainQuest != null) return;
                CurrentMainQuest = quest;
            }
            
            if(quest is IObtainItemQuest obtainItemQuest)
                OnObtainItemQuestReceived?.Invoke(obtainItemQuest);
        }

        public Quest OnQuestDoneHandler(QuestID questID)
        {
            var quest = CurrentMainQuest != null && CurrentMainQuest.ID == questID
                ? CurrentMainQuest
                : CurrentSideQuests.Find(questToFind => questToFind.ID == questID);

            if (quest != null)
                quest.UnlockReward();

            return quest;
        }
        
        private void OnQuestCompleteHandler(QuestID questID)
        {
            var quest = CurrentMainQuest.ID == questID
                ? CurrentMainQuest
                : CurrentSideQuests.Find(questToFind => questToFind.ID == questID);

            if (quest == null || !quest.QuestData.IsRewardAvailable) return;

            if (quest == CurrentMainQuest)
            {
                Object.Destroy(CurrentMainQuest);
                CurrentMainQuest = null;
            }
            else
            {
                Object.Destroy(quest);
                CurrentSideQuests.Remove(quest);
            }
        }

        public void OnItemQuestDoneHandler(QuestID questId, bool reward)
        {
            if(reward)
                OnQuestCompleteHandler(questId);
            else OnQuestDoneHandler(questId);
        }

        public void OnKillQuest(string name)
        {
            if (CurrentMainQuest is IKillQuest killQuest && killQuest.TargetName == name)
            {
                OnQuestDoneHandler(CurrentMainQuest.ID);
            }
            else
            {
                var killSideQuest = CurrentSideQuests.Find(quest => quest is IKillQuest);
                
                if(killSideQuest != null && (killSideQuest as IKillQuest).TargetName == name)
                    OnQuestDoneHandler(killSideQuest.ID);
            }
        }

        public bool OnIfQuestAllowsHandler(QuestID id)
        {
            if (CurrentMainQuest == null) return false;
            return (CurrentMainQuest.ID == id && CurrentMainQuest.QuestData.IsRewardAvailable) || CurrentSideQuests.Any(quest => quest.ID == id && quest.QuestData.IsRewardAvailable);
        }
    }

}