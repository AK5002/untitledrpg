﻿using System;
using System.Collections.Generic;

namespace Core.Game.Relations
{
    public sealed class Relationship
    {
        public static Relationship Instance { get; } = new Relationship();

        private Relationship()
        {
            FriendlyRelations = new List<Relation>()
            {
                new Relation(Factions.Player, Factions.Humans).Sort(),
                new Relation(Factions.Player, Factions.Elves).Sort(),
                new Relation(Factions.Humans, Factions.Elves).Sort()
            };
            HostileRelations = new List<Relation>()
            {
                new Relation(Factions.Player,Factions.Evil).Sort(),
                new Relation(Factions.Evil,Factions.Elves).Sort(),
                new Relation(Factions.Evil,Factions.Humans).Sort(),
            };
        }

        private List<Relation> FriendlyRelations;
        private List<Relation> HostileRelations;

        public RelationType GetRelation(Factions faction1, Factions faction2)
        {
            var sortedTuple = new Relation(faction1, faction2).Sort();
            RelationType returnValue = RelationType.Neutral;
            if (FriendlyRelations.Contains(sortedTuple))
                returnValue = RelationType.Friendly;
            if (HostileRelations.Contains(sortedTuple))
                returnValue = RelationType.Hostile;

            return returnValue;
        }

        public void SetRelation(Factions faction1, Factions faction2, RelationType type)
        {

            var sortedTuple = new Relation(faction1, faction2).Sort();

            if (FriendlyRelations.Contains(sortedTuple))
                FriendlyRelations.Remove(sortedTuple);
            if (HostileRelations.Contains(sortedTuple))
                HostileRelations.Remove(sortedTuple);

            switch (type)
            {
                case RelationType.Friendly:
                    FriendlyRelations.Add(sortedTuple);

                    break;
                case RelationType.Hostile:
                    HostileRelations.Add(sortedTuple);
                    break;
            }
        }

        public void ChangeRelation(Factions faction1, Factions faction2, int dir)
        {
            var sortedTuple = new Relation(faction1, faction2).Sort();

            if (FriendlyRelations.Contains(sortedTuple))
                FriendlyRelations.Remove(sortedTuple);
            if (HostileRelations.Contains(sortedTuple))
                HostileRelations.Remove(sortedTuple);

            var type = GetRelation(faction1, faction2) - dir;
            
            switch (type)
            {
                case RelationType.Friendly:
                    FriendlyRelations.Add(sortedTuple);

                    break;
                case RelationType.Hostile:
                    HostileRelations.Add(sortedTuple);
                    break;
            }
        }
    }

    public class Relation : Tuple<Factions, Factions>
    {
        public Relation(Factions item1, Factions item2) : base(item1, item2)
        {
        }

        public Relation Sort()
        {
            var sorted = new Relation((Factions) Math.Min((int) Item1, (int) Item2),
                (Factions) Math.Max((int) Item1, (int) Item2));

            return sorted;
        }
    }
    
    public enum Factions
    {
        Player,
        Humans,
        Elves,
        Evil,
    }
    
    public enum RelationType
    {
        Friendly,
        Neutral,
        Hostile,
    }
}