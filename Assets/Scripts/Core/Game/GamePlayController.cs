﻿using System;
using System.Collections.Generic;
using System.Linq;
using DialogueSystem;
using Core.Game.Interactables;
using Core.Game.Items;
using Core.Game.Levels;
using Core.Game.Levels.Areas;
using Core.Game.NPC;
using Core.Game.PlayerCharacter;
using Core.Game.Relations;
using Core.Managers;
using PuzzleSystem;
using InGameNotificationSystem;
using QuestSystem;
using Services.DependencyInjection;
using SFX;
using UI;
using UI.PopUps.DialoguePopUp;
using UI.PopUps.InventoryMenu;
using UI.PopUps.QuestMenu;
using UI.PopUps.ScrollPopUp;
using UI.Views;
using UnityEngine;

namespace Core.Game
{
    public class GamePlayController : MonoBehaviour
    {
        [Header("Managers")] [SerializeField] private InputManager m_InputManager;
        [SerializeField] private UIManager m_UIManager;

        [Header("Controllers")] [SerializeField]
        private InteractionController m_InteractionController;
        [SerializeField] private LevelController m_LevelController;
        [SerializeField] private DialogueController m_DialogueController;
        [SerializeField] private PuzzleController m_PuzzleController;
        [SerializeField] private AudioController m_AudioController;
        [SerializeField] private NotificationController m_NotificationController;
        [SerializeField] private QuestController m_QuestController;

        [Header("Player")] [SerializeField] private Player m_Player;

        private Level CurrentLevel => m_LevelController.CurrentLevel;
        private AreaController CurrentAreaController { get; set; }

        private GamePlayView GamePlayView;
        private DialogPopUp DialogueMenu;

        private List<IObtainItemQuest> ObtainQuests = new List<IObtainItemQuest>();
        public event Action OnNewLevelLoaded;

        public void Setup()
        {
            m_Player.Setup(m_InputManager);
            m_InputManager.Setup();
            m_UIManager.Setup();
            
            m_InputManager.OnUseButtonPressed += OnUseButtonHandler;
            m_InputManager.m_UseButton.gameObject.SetActive(false);
            m_Player.OnPlayerDead += () =>  OnGameOver(false);
            m_Player.OnOpenScroll += content =>
            {
                m_UIManager.PopUpController.Show<ScrollPopUp, ScrollPopUpData>(new ScrollPopUpData(content));
            };
            m_Player.OnNPCKilled += (name) => m_QuestController.OnKillQuest(name); 
            m_InteractionController.OnInteraction += OnInteractionHandler;
            m_DialogueController.OnNodeChanged += UpdateDialogue;
            m_DialogueController.OnEndDialogue += OnEndDialogueHandler;
            m_DialogueController.OnTrade += OnDialogueTradeHandler;
            m_DialogueController.OnQuest += (quest) =>
            {
                m_QuestController.AddQuest(quest);
                m_NotificationController.AddNotification("Received Quest: " + quest.QuestData.Name);
            };
            m_DialogueController.OnIfQuestAllows += m_QuestController.OnIfQuestAllowsHandler;
            m_DialogueController.OnTradeAllowed += OnTradeAllowedHandler;
            m_DialogueController.OnChangeReputation += OnChangeReputationHandler;
            m_QuestController.OnObtainItemQuestReceived += (quest) => ObtainQuests.Add(quest);
            LoadLevel(0, null);
            UIInit();
            GamePlayView =
                m_UIManager.ViewController.Show<GamePlayView, GamePlayViewData>(new GamePlayViewData(m_Player.Stats));
        }

        private void OnChangeReputationHandler(int dir, Factions npcFaction)
        {
          Relationship.Instance.ChangeRelation(m_Player.Faction,npcFaction,dir);
        }

        private bool OnTradeAllowedHandler(List<Item> items)
        {
            foreach (var item in items)
            {
                if (m_Player.Inventory.AllItems.All(itemToFind => itemToFind.ID != item.ID))
                    return false;
            }
            return true;
        }

        private void UIInit()
        {
            m_UIManager.OnInventoryButton = () =>
                m_UIManager.PopUpController.Show<PlayerInventoryPopUp, InventoryPopUpData>(
                    new InventoryPopUpData(m_Player.Inventory,null));
            m_UIManager.OnInventoryLootCell = (item) => m_Player.Inventory.AddItem(item);
            m_UIManager.OnLootGoldButton = (sack) => { m_Player.Inventory.AddItem(sack); };
            m_UIManager.OnDirectionChosen += (direction) =>
            {
                m_DialogueController.OnDirectionChosenHandler(direction);
            };
            m_UIManager.OnStoreCellHandler = OnStoreCellHandler;
            m_UIManager.OnDialogueClosed = m_DialogueController.AbortDialogue;
        }
        private void OnDialogueTradeHandler(List<Item> items, Inventory npcInventory, bool toPlayer,QuestID questId, bool reward)
        {
            Debug.Log(items[0]);
            foreach (var item in items)
            {
                if (toPlayer)
                {
                    m_Player.Inventory.AddItem(Instantiate(item));
                    npcInventory.RemoveItem(item);
                    var obtainquest = ObtainQuests.Find(quest => quest.TargetItem.ID == item.ID);
                    if (obtainquest != null)
                    {
                        ObtainQuests.Remove(obtainquest);
                        m_QuestController.OnQuestDoneHandler(obtainquest.ID);
                    }
                }
                else
                {
                    npcInventory.AddItem(Instantiate(item));
                    m_Player.Inventory.RemoveItem(item);
                }
            }
            
            m_QuestController.OnItemQuestDoneHandler(questId,reward);
        }

        private void OnEndDialogueHandler(ExitType exitType, IDialogueOwner owner)
        {
            if (exitType == ExitType.Combat)
            {
                if (owner is IFightable fightable)
                {
                    fightable.CurrentEnemy = m_Player.transform;
                }
            }

            if (exitType == ExitType.Trade)
            {
                var store = m_InteractionController.AvailableForInteract.GetComponent<StoreInteractable>();
                
                if(store!=null)
                    m_UIManager.PopUpController.Show<StorePopUp,InventoryPopUpData>(new InventoryPopUpData(m_Player.Inventory,store.StoreInventory));
            }
            
            m_UIManager.PopUpController.Hide(DialogueMenu);
        }

        private void LoadLevel(int id, LevelData data)
        {
            m_LevelController.LoadLevel(0, null);
            CurrentAreaController = CurrentLevel.m_AreaController;
            OnNewLevelLoaded?.Invoke();
        }

        private void OnUseButtonHandler()
        {
            if (m_InteractionController.AvailableForInteract != null)
            {
                var interactable = m_InteractionController.AvailableForInteract.GetComponent<AbstractInteractable>();

                if (interactable == null || !interactable.enabled) return;


                if (interactable.Group == typeof(ItemInteractable))
                {
                    var item = interactable.ItemInteract();
                    var obtainquest = ObtainQuests.Find(quest => quest.TargetItem.ID == item.ID);
                    if (obtainquest != null)
                    {
                        ObtainQuests.Remove(obtainquest);
                        m_NotificationController.AddNotification("Quest Complete: " + m_QuestController.OnQuestDoneHandler(obtainquest.ID).QuestData.Name);
                    }
                    m_Player.Inventory.AddItem(item);
                }

                if (interactable.Group == typeof(InventoryInteractable))
                {
                    var inventory = interactable.InventoryInteract();
                    m_UIManager.PopUpController.Show<InventoryLootPopUp, InventoryPopUpData>(
                        new InventoryPopUpData(null,inventory));
                }

                if (interactable.Group == typeof(DialogueInteractable))
                {
                    var dialogueTree = interactable.DialogueInteract();

                    DialogueMenu = m_UIManager.PopUpController.Show<DialogPopUp, UIData>(null);
                    m_DialogueController.BeginDialogue(dialogueTree);
                }

                if (interactable.Group == typeof(PuzzleInteractable))
                {
                    m_PuzzleController.BeginPuzzle(interactable.PuzzleInteract());
                }
                else
                {
                    interactable.Interact();
                }
            }
        }

        private void OnInteractionHandler(AbstractInteractable interactable, bool calledFromEnter)
        {
            if (interactable == null)
            {
                m_InputManager.m_UseButton.gameObject.SetActive(false);
                return;
            }

            if (!interactable.enabled) return;
            
            m_InputManager.m_UseButton.gameObject.SetActive(calledFromEnter);
            m_InputManager.m_UseButtonText.text = interactable.AvailableAction;
        }
        

        private void UpdateDialogue(string npcResponse, ResponseType type,List<DialogueMenuResponse> newResponses)
        {
            if (m_UIManager.PopUpController.CurrentPopUp == DialogueMenu)
                DialogueMenu.UpdateMenu(npcResponse, type, newResponses);
        }
        
        private void OnStoreCellHandler(Item item, bool buy)
        {
            var store = m_InteractionController.AvailableForInteract.GetComponent<StoreInteractable>();
            if (store == null) return;

            var price = store.GetPriceOfItem(item);
            var sack = ScriptableObject.CreateInstance<GoldSack>();
            sack.Cost = buy ? item.Cost : price;

            if (buy)
            {
                if(m_Player.Inventory.GoldAmount < price) return;

                m_Player.Inventory.AddItem(item);
                 m_Player.Inventory.RemoveItem(sack);
                 
                 store.StoreInventory.RemoveItem(item);
                 store.StoreInventory.AddItem(sack);
            }
            else
            {
                if(store.StoreInventory.GoldAmount < price) return;
                
                m_Player.Inventory.RemoveItem(item);
                m_Player.Inventory.AddItem(sack);
                 
                store.StoreInventory.AddItem(item);
                store.StoreInventory.RemoveItem(sack);
            }
        }
        
        private void EnterMainMenu()
        {
        }

        private void EnterGame()
        {
        }

        private void PauseGame()
        {
        }

        private void ResumeGame()
        {
        }

        private void OnGameOver(bool calledFromRestart)
        {
            Debug.Log($"GameOver {calledFromRestart}");
        }
    }
}