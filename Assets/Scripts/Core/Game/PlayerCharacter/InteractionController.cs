﻿using System;
using Core.Game.Interactables;
using UnityEngine;

namespace Core.Game.PlayerCharacter
{
    public class InteractionController : MonoBehaviour
    {
        public GameObject AvailableForInteract { get; private set; }
        public event Action<AbstractInteractable, bool> OnInteraction;


        public void OnTriggerStay2D(Collider2D other)
        {
            if(AvailableForInteract != null) return;
            
            AvailableForInteract = other.gameObject;
            
            var interactable = other.gameObject.GetComponent<AbstractInteractable>();
            
            OnInteraction?.Invoke(interactable, interactable != null);
        }

        public void OnTriggerExit2D(Collider2D other)
        {
            AvailableForInteract = null;
            
            var interactable = other.gameObject.GetComponent<AbstractInteractable>();
            if(interactable != null)
                OnInteraction?.Invoke(interactable, false);
        }
    }
}