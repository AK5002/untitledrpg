﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Game.Relations;
using Core.Game.Weapons;
using Core.Managers;
using DG.Tweening;
using UnityEngine;

namespace Core.Game.PlayerCharacter
{
    public class Player : Character
    {
        [SerializeField] private WeaponHolder m_WeaponHolder;
        
        public event Action OnPlayerDead;
        public event Action<string> OnOpenScroll;
        public event Action<string> OnNPCKilled;

        private Tweener tweener;

        public void Setup(IPlayerInput playerInput)
        {
            InitializeCharacter();
            m_WeaponHolder.Setup();
            m_Inventory.OnWeaponChanged = weapon => m_WeaponHolder.ChangeWeapon(weapon);
            m_Inventory.OnScrollOpened = content => OnOpenScroll?.Invoke(content);
            m_WeaponHolder.OnWeaponUsed += (stamina, mana, arrow) =>
            {
                Stats.IncrementStats(0, -Mathf.Abs(stamina), -Mathf.Abs(mana));
                m_Inventory.ArrowAmount -= Mathf.Abs(arrow);
            };
            m_WeaponHolder.OnWeaponHitCharacter += (character,dead) =>
            {
                Relationship.Instance.SetRelation(Faction, character.Faction, RelationType.Hostile);
                if(dead)
                    OnNPCKilled?.Invoke(character.CharacterName);
            };
            Stats = new Stats(100, 100, 100);
            
            playerInput.OnPlayerMoveByDirection += Move;
            playerInput.OnFireButtonDown += Fire;
            playerInput.OnFireButtonUp += StopFire;
        }

        private void Update()
        {

            var staminaIncrement = CurrentVelocity.x > 5 || CurrentVelocity.y > 5 ? -0.2f :
                CurrentVelocity == Vector2.zero ? 0.1f : 0;

            if (m_Inventory.IsOverweight) staminaIncrement = -1;

            if (Stats.Stamina <= 0)
            {
                m_Speed = 0;
                staminaIncrement = CurrentVelocity == Vector2.zero ? 0.1f : -0.2f;
            }

            else if (Stats.Stamina - staminaIncrement >= 5)
            {
                m_Speed = 7;
            }


            Stats.IncrementStats(0.1f, staminaIncrement, 0.2f);
            

            CurrentVelocity = Vector2.zero;
        }

        private void FixedUpdate()
        {
            m_RigidBody.angularVelocity = 0;
            m_RigidBody.velocity = Vector2.zero;
        }
        
        private void Move(Vector3 direction)
        {
            CurrentVelocity = new Vector2(Mathf.Abs(direction.x), Mathf.Abs(direction.y)) * m_Speed;
            
            // tweener?.Kill();
            // tweener = transform.DOMove(transform.position + direction.normalized, Time.deltaTime);
            
            transform.position = Vector3.LerpUnclamped(transform.position,
                transform.position + ( direction.normalized * m_Speed),
                Time.deltaTime);

            m_InteractionTransform.up = direction;
        }

        private void Fire()
        {
            if (m_WeaponHolder.m_WeaponData == null) return;

            if (m_WeaponHolder.m_WeaponData.StaminaCost > Stats.Stamina ||
                m_WeaponHolder.m_WeaponData.ManaCost > Stats.Mana ||
                m_WeaponHolder.m_WeaponData.ArrowCost > m_Inventory.ArrowAmount)
                return;

            m_WeaponHolder.Fire();
        }

        private void StopFire()
        {
            m_WeaponHolder.StopFire();
        }

        protected override void OnDead()
        {
            OnPlayerDead?.Invoke();
        }
    }
}