﻿using System;
using Core.Game.Items;
using UnityEngine;

namespace Core.Game.Interactables
{
    public class ItemInteractable : AbstractInteractable
    {
        [SerializeField] public Item Item;

        public override Type Group => typeof(ItemInteractable);

        public override void Setup()
        {
        }

        public override Item ItemInteract()
        {
            Debug.Log("interact");
            OnInteractionOver();
            return Instantiate(Item);
        }
        
        protected override void OnInteractionOver()
        {
            OnInteractableDestroyed.Invoke(this);
            Destroy(gameObject);
        }

        private void Update()
        {
            
        }
    }
}