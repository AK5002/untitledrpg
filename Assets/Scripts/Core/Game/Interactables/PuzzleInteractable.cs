using System;
using PuzzleSystem;
using UnityEngine;

namespace Core.Game.Interactables
{
    public class PuzzleInteractable : AbstractInteractable
    {
        [SerializeField] private PuzzleHolder m_Holder;
        
        public override Type Group => typeof(PuzzleInteractable);
        
        public override void Setup()
        {
            
        }

        public override PuzzleHolder PuzzleInteract()
        {
            OnInteractionOver();
            return m_Holder;
        }
        
        protected override void OnInteractionOver()
        {
            OnInteractableDestroyed?.Invoke(this);
            Destroy(this);
        }
    }
}