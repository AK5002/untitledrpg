using System;
using System.Collections.Generic;
using DialogueSystem;
using Core.Game.NPC;
using QuestSystem;
using UnityEngine;


namespace Core.Game.Interactables
{
    public class DialogueInteractable : AbstractInteractable
    {
        [SerializeField] private NonPlayableCharacter Owner;
        [SerializeField] private List<DialogueTree> m_Trees;
        
        public override Type Group => typeof(DialogueInteractable);
        
        public override void Setup()
        {
        }

        public override DialogueTree DialogueInteract()
        {
            var returnTree = Instantiate(m_Trees[0]);
            returnTree.Setup(Owner, Owner.Inventory);
            
            return returnTree;
        }
        
    }
}