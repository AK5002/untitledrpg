﻿using System;
using DialogueSystem;
using Core.Game.Items;
using PuzzleSystem;
using QuestSystem;
using UnityEngine;

namespace Core.Game.Interactables
{
    public abstract class AbstractInteractable : MonoBehaviour
    {

        [SerializeField] private string m_AvailableAction;

        public abstract Type Group { get; }
        public string AvailableAction => m_AvailableAction;
        
        public Action<AbstractInteractable> OnInteractableDestroyed;
        
        public abstract void Setup();
        
        public virtual void Interact()
        {
        }
        
        public virtual Item ItemInteract()
        {
            return null;
        }
        
        public virtual Inventory InventoryInteract()
        {
            return null;
        }

        public virtual DialogueTree DialogueInteract()
        {
            return null;
        }

        public virtual PuzzleHolder PuzzleInteract()
        {
            return null;
        }

        protected virtual void OnInteractionOver()
        {
            
        }
    }
}
