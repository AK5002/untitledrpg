﻿using System;
using UnityEngine;

namespace Core.Game.Interactables
{
    public class InventoryInteractable : AbstractInteractable
    {
        [SerializeField] private Inventory m_Inventory;

        public override Type Group => typeof(InventoryInteractable);

        public override void Setup()
        {
        }

        public override Inventory InventoryInteract()
        {
            OnInteractionOver();
            return m_Inventory;
        }

        protected override void OnInteractionOver()
        {
            
        }
    }
}