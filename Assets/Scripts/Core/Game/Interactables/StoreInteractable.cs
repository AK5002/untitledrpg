using System;
using Core.Game.Items;
using Core.Game.Items.Potions;
using UnityEngine;

namespace Core.Game.Interactables
{
    public class StoreInteractable : AbstractInteractable
    {
        [SerializeField] private Inventory m_StoreInventory;
        [SerializeField] private StoreSpecialty m_Specialty;

        public Inventory StoreInventory => m_StoreInventory;

        public override Type Group => typeof(StoreInteractable);

        public override void Setup()
        {

        }

        public int GetPriceOfItem(Item item)
        {
            var price = (item.Cost * 9) / 10 ;
            if (m_Specialty == StoreSpecialty.Herbalist)
            {
                if (item.ID.Type != 1)
                    price = price / 2;
            }
            
            return price;
        }
    }
    
    public enum StoreSpecialty
    {
        Herbalist,
        Mage,
        Smith,
    }
}