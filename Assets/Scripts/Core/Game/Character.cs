﻿using Core.Game.Levels.Areas;
using Core.Game.Relations;
using Core.Game.Weapons;
using UnityEngine;

namespace Core.Game
{
    public abstract class Character : MonoBehaviour, IApplyDamage
    {
        [SerializeField] private string m_CharacterName;
        [SerializeField] protected Transform m_InteractionTransform;
        [SerializeField] protected Inventory m_Inventory;
        [SerializeField] protected Rigidbody2D m_RigidBody;
        [SerializeField] protected float m_Speed = 5f;
        [SerializeField] protected Factions m_Faction;


        public Stats Stats { get; protected set; }
        public Inventory Inventory => m_Inventory;
        protected Vector2 CurrentVelocity;
        

        public Factions Faction => m_Faction;
        public string CharacterName => m_CharacterName;


        protected abstract void OnDead();
        
        protected void InitializeCharacter()
        {
            m_Inventory.Setup();
            m_Inventory.OnPotionDrunk = (health, stamina, mana) => Stats.IncrementStats(health, stamina, mana);
            
        }

        public virtual bool TakeDamage(float healthDamage, float staminaDamage, float manaDamage)
        {
            Stats.IncrementStats(-Mathf.Abs(healthDamage), -Mathf.Abs(staminaDamage), -Mathf.Abs(manaDamage));
            
            if(Stats.Health<=0)
                OnDead();

            return Stats.Health <= 0;
        }


        public void OnTriggerEnter2D(Collider2D other)
        {
            other.gameObject.GetComponent<Area>()?.OnCharacterAreaEnter(this);
        }
    }

    public interface IApplyDamage
    {
        bool TakeDamage(float healthDamage, float staminaDamage, float manaDamage);
        Factions Faction { get; }
        string CharacterName { get; }
    }
}