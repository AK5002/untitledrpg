using System.Collections.Generic;
using UnityEngine;

namespace Core.Game.NPC
{
    public interface IProtectionSubject
    {
        bool IsBeingAttacked { get; set; }
        List<IProtectionObserver> Observers { get; set; }
        
        Transform AttackingCharacter { get; set; }
        void Attach(IProtectionObserver observer);
        void Detach(IProtectionObserver observer);

        void Notify();
    }

    public interface IProtectionObserver
    {
        bool CanRespond { get; set; }
        void UpdateObserver(IProtectionSubject subject);
    }
}