using System.Collections;
using UnityEngine;

namespace Core.Game
{
    public interface IFightable
    {
        Transform CurrentEnemy { get; set; }
        bool SearchForEnemy { get; set; }
        void Fire();
    }
}