using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Relations;
using UnityEngine;

namespace Core.Game.NPC
{
    public class Civilian : NonPlayableCharacter, INonPlayable, IProtectionSubject
    {
        public bool IsEnemyNearBy
        {
            get => false;
            set => value = false;
        }

        public bool CanAttack
        {
            get => false;
            set => value = false;
        }

        public bool MustProtect
        {
            get => false;
            set => value = false;
        }

        public bool SearchForEnemy { get; set; }


        public bool IsBeingAttacked { get; set; }
        public List<IProtectionObserver> Observers { get; set; }
        public Transform AttackingCharacter { get; set; }

        public IEnumerator Hunt()
        {
            yield break;
        }

        public IEnumerator Attack()
        {
            yield break;
        }

        public IEnumerator Protect()
        {
            yield break;
        }

        public IEnumerator Search()
        {
            while (true)
            {
                List<RaycastHit2D> hitResults =
                    new List<RaycastHit2D>(Physics2D.CircleCastAll((Vector2) transform.position, 10f, Vector2.zero));
                if (hitResults.Count != 0)
                {
                    IsEnemyNearBy = true;
                    IsBeingAttacked = true;
                    IEnumerable<RaycastHit2D> query = hitResults.OrderBy(element =>
                        Vector3.Distance(element.transform.position, transform.position));
                    AttackingCharacter = query.Take(1).ToArray()[0].transform;
                    Notify();
                }
                else
                {
                    IsEnemyNearBy = false;
                    IsBeingAttacked = false;
                    AttackingCharacter = null;
                    Notify();
                }


                yield return null;
            }
        }
        public void StartStateAction(IEnumerator action)
        {
            StartCoroutine(action);
        }

        public void StopStateAction(IEnumerator action)
        {
            StopCoroutine(action);
        }

        public override bool TakeDamage(float healthDamage, float staminaDamage, float manaDamage)
        {
            SearchForEnemy = true;
            
            return base.TakeDamage(healthDamage, staminaDamage, manaDamage);
        }

        public void Attach(IProtectionObserver observer)
        {
            Observers?.Add(observer);
        }

        public void Detach(IProtectionObserver observer)
        {
            if (Observers?.Count == 0) return;

            Observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var obserever in Observers)
            {
                obserever.UpdateObserver(this);
            }
        }

        protected override void InitializeStats()
        {
            Stats = new Stats(50, 50, 0);
        }

        protected override void InitializeStateMachine()
        {
            StateMachine = new NPCStateMachine(this);
        }

        protected override void TypeDependentInitialize()
        {
            Observers = new List<IProtectionObserver>();
        }
    }
}