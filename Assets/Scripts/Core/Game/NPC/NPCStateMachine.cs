using UnityEngine;

namespace Core.Game.NPC
{
    public class NPCStateMachine
    {
        private NPCState CurrentState;
        public readonly INonPlayable NonPlayable;

        public NPCStateMachine(INonPlayable nonPlayable)
        {
            NonPlayable = nonPlayable;
            SetState(new IdleState(this));
        }

        public void SetState(NPCState state)
        {
            if (CurrentState != null)
            {
               NonPlayable.StopStateAction(CurrentState.StateAction());
            }
            
            CurrentState = state;
            NonPlayable.StartStateAction(CurrentState.StateAction());
            
        }
    }
}