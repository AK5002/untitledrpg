using Pathfinding;
using UnityEngine;

namespace Core.Game.NPC
{
    public class NPCPathfinding
    {
        public Transform Target;
        
        public float NextWayPointDistance = 3f;
        public int CurrentWayPoint = 0;
        public bool ReachedEndOfPath = false;
        
        public Path CurrentPath;

        public NPCPathfinding()
        {
        }
    }
}