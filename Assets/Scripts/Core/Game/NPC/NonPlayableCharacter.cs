﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Game.Interactables;
using Core.Game.Relations;
using Pathfinding;
using UnityEngine;

namespace Core.Game.NPC
{
    public abstract class NonPlayableCharacter : Character, IDialogueOwner
    {
        [SerializeField] private int m_Id;
        [SerializeField] protected Seeker m_Seeker;
        [SerializeField] protected InventoryInteractable m_InventoryInteractable;
        
        public event Action<NonPlayableCharacter> OnNPCDead;
        protected NPCStateMachine StateMachine;
        protected NPCPathfinding PathFinding;
        
        public int Id => m_Id;
        public InventoryInteractable InventoryInteractable => m_InventoryInteractable;

        public void Setup(NonPlayableCharacter character)
        {
            if(m_InventoryInteractable != null)
                m_InventoryInteractable.enabled = false;
            InitializeCharacter();
            InitializeStats();
            InitializePathFinding();
            InitializeStateMachine();
            TypeDependentInitialize();
        }

        protected virtual void InitializePathFinding()
        {
        }

        protected void OnPathComplete(Path path)
        {
            if (path.error) return;

            PathFinding.CurrentPath = path;
            PathFinding.CurrentWayPoint = 0;
        }
     
        protected override void OnDead()
        {
            OnNPCDead?.Invoke(this);
            enabled = false;
            m_InventoryInteractable.enabled = true;
            transform.eulerAngles += new Vector3(0,0,180);
        }

        protected virtual void InitializeStats()
        {
            Stats = new Stats(50,50,0);
        }

        protected virtual void InitializeStateMachine()
        {
            
        }

        protected virtual void TypeDependentInitialize()
        {
            
        }

        private void Update()
        {

            if (PathFinding == null || PathFinding.CurrentPath == null)
                return;
            
            PathFinding.ReachedEndOfPath =PathFinding.CurrentWayPoint >= PathFinding.CurrentPath.vectorPath.Count;
            
        }

        private void FixedUpdate()
        {
            m_RigidBody.angularVelocity = 0;
            m_RigidBody.velocity = Vector2.zero;
        }
        
    }

    public class NPCData
    {
    }
    
    public interface INonPlayable
    {
        bool IsEnemyNearBy { get; set; }
        bool CanAttack { get; set; }
        bool MustProtect { get; set; }
        bool SearchForEnemy { get; set; }
        IEnumerator Hunt();
        IEnumerator Attack();
        IEnumerator Protect();
        IEnumerator Search();
        void StartStateAction(IEnumerator action);
        void StopStateAction(IEnumerator action);
    }

    public interface IDialogueOwner
    {
        string CharacterName { get; }
        Factions Faction { get; }
    }
}