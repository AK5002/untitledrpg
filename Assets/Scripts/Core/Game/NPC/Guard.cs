using System.Collections;

namespace Core.Game.NPC
{
    public class Guard : Soldier, IProtectionObserver
    {
        public bool CanRespond { get; set; } = true;
        public void UpdateObserver(IProtectionSubject subject)
        {
            if (CanRespond && subject.IsBeingAttacked)
            {
                MustProtect = true;
                CurrentEnemy = subject.AttackingCharacter;
            }
            else
            {
                CurrentEnemy = CurrentEnemy;
                MustProtect = false;
            }
        }
    }
}