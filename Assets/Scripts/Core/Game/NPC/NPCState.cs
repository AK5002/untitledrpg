using System.Collections;
using UnityEngine;

namespace Core.Game.NPC
{
    public abstract class NPCState
    {
        protected readonly NPCStateMachine StateMachine;

        public NPCState(NPCStateMachine stateMachine)
        {
            StateMachine = stateMachine;
        }


        public abstract IEnumerator StateAction();
    }

    public class IdleState : NPCState
    {
        public IdleState(NPCStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override IEnumerator StateAction()
        {
            while (true)
            {
                if (StateMachine.NonPlayable.SearchForEnemy)
                {
                    StateMachine.SetState(new SearchState(StateMachine));
                    yield break;
                }
                if (StateMachine.NonPlayable.IsEnemyNearBy)
                {
                    StateMachine.SetState(new HuntState(StateMachine));
                    yield break;
                }

                if (StateMachine.NonPlayable.CanAttack)
                {
                    StateMachine.SetState(new AttackState(StateMachine));
                    yield break;
                }

                if (StateMachine.NonPlayable.MustProtect)
                {
                    StateMachine.SetState(new ProtectState(StateMachine));
                    yield break;
                }

                yield return null;
            }
        }
    }

    public class HuntState : NPCState
    {
        public HuntState(NPCStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override IEnumerator StateAction()
        {
            StateMachine.NonPlayable.StartStateAction(StateMachine.NonPlayable.Hunt());
            while (true)
            {
                if (StateMachine.NonPlayable.CanAttack)
                {
                    StateMachine.SetState(new AttackState(StateMachine));
                    yield break;
                }

                if (!StateMachine.NonPlayable.IsEnemyNearBy)
                {
                    StateMachine.SetState(new IdleState(StateMachine));
                    yield break;
                }

                if (StateMachine.NonPlayable.MustProtect)
                {
                    StateMachine.SetState(new ProtectState(StateMachine));
                    yield break;
                }

                yield return null;
            }
        }
    }

    public class AttackState : NPCState
    {
        public AttackState(NPCStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override IEnumerator StateAction()
        {
            StateMachine.NonPlayable.StartStateAction(StateMachine.NonPlayable.Attack());
            while (true)
            {
                if (!StateMachine.NonPlayable.CanAttack)
                {
                    StateMachine.SetState(new HuntState(StateMachine));
                    yield break;
                }

                yield return null;
            }
        }
    }

    public class ProtectState : NPCState
    {
        public ProtectState(NPCStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override IEnumerator StateAction()
        {
            StateMachine.NonPlayable.StartStateAction(StateMachine.NonPlayable.Protect());
            while (true)
            {
                if (!StateMachine.NonPlayable.MustProtect)
                {
                    StateMachine.SetState(new HuntState(StateMachine));
                    yield break;
                }

                if (StateMachine.NonPlayable.CanAttack)
                {
                    StateMachine.SetState(new AttackState(StateMachine));
                    yield break;
                }
                
                yield return null;
            }
        }
    }

    public class SearchState : NPCState
    {
        public SearchState(NPCStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override IEnumerator StateAction()
        {
            StateMachine.NonPlayable.StartStateAction(StateMachine.NonPlayable.Search());
            while (true)
            {
                if (StateMachine.NonPlayable.IsEnemyNearBy)
                {
                    StateMachine.SetState(new HuntState(StateMachine));
                    yield break;
                }

                if (!StateMachine.NonPlayable.SearchForEnemy)
                {
                    StateMachine.SetState(new IdleState(StateMachine));
                    yield break;
                }

                yield return null;
            }
        }
    }
}