using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Relations;
using Core.Game.Weapons;
using DG.Tweening;
using Pathfinding;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace Core.Game.NPC
{
    public class Soldier : NonPlayableCharacter, IFightable, INonPlayable
    {
        [SerializeField] protected WeaponHolder m_WeaponHolder;

        public Transform CurrentEnemy { get; set; }
        public bool SearchForEnemy { get; set; }

        public bool IsEnemyNearBy { get; set; }
        public bool CanAttack { get; set; }
        public bool MustProtect { get; set; }

        public void Fire()
        {
            if (m_WeaponHolder.m_WeaponData == null) return;

            if (m_WeaponHolder.m_WeaponData.StaminaCost > Stats.Stamina ||
                m_WeaponHolder.m_WeaponData.ManaCost > Stats.Mana ||
                m_WeaponHolder.m_WeaponData.ArrowCost > m_Inventory.ArrowAmount)
                return;

            m_WeaponHolder.Fire();
        }

        public void OnCollisionStay2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<Character>() != null && other.gameObject.transform == CurrentEnemy)
            {
                var faction = other.gameObject.GetComponent<Character>().Faction;
                if (Relationship.Instance.GetRelation(faction, Faction) == RelationType.Hostile)
                    CanAttack = true;
            }
        }

        public void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<Character>()?.transform == CurrentEnemy)
            {
                var faction = other.gameObject.GetComponent<Character>().Faction;
                if (Relationship.Instance.GetRelation(faction, Faction) == RelationType.Hostile)
                    CanAttack = false;
            }
        }

        protected override void InitializeStats()
        {
            Stats = new Stats(75, 100, 75);
        }

        protected override void InitializeStateMachine()
        {
            StateMachine = new NPCStateMachine(this);
        }

        protected override void InitializePathFinding()
        {
            PathFinding = new NPCPathfinding();
            InvokeRepeating(nameof(UpdatePath), 0, 5f);
        }

        protected void UpdatePath()
        {
            if (CurrentEnemy != null && m_Seeker.IsDone())
            {
                m_Seeker.StartPath(transform.position, CurrentEnemy.position, OnPathComplete);
            }
        }

        protected override void TypeDependentInitialize()
        {
            m_WeaponHolder.Setup();
            m_Inventory.OnWeaponChanged = weapon => m_WeaponHolder.ChangeWeapon(weapon);
            m_WeaponHolder.OnWeaponUsed += (stamina, mana, arrow) =>
            {
                Stats.IncrementStats(0, -Mathf.Abs(stamina), -Mathf.Abs(mana));
                m_Inventory.ArrowAmount -= Mathf.Abs(arrow);
            };
            m_WeaponHolder.OnWeaponHitCharacter += (character,dead) =>
                Relationship.Instance.SetRelation(Faction, character.Faction, RelationType.Hostile);
        }

        public override bool TakeDamage(float healthDamage, float staminaDamage, float manaDamage)
        {
            return base.TakeDamage(healthDamage, staminaDamage, manaDamage);
        }

        public virtual IEnumerator Hunt()
        {
            if (CurrentEnemy == null) yield break;

            while (true)
            {
                if (PathFinding.CurrentPath != null && !PathFinding.ReachedEndOfPath)
                {
                    Debug.Log(PathFinding.CurrentPath.vectorPath.Last());
                    Vector3 direction = PathFinding.CurrentPath.vectorPath[PathFinding.CurrentWayPoint] -
                                        transform.position;

                    transform.DOMove((Vector2)PathFinding.CurrentPath.vectorPath[PathFinding.CurrentWayPoint], Time.deltaTime);
                    // transform.position = Vector3.LerpUnclamped(transform.position,
                    //     transform.position + ( (Vector3)(Vector2)direction.normalized  * m_Speed),
                    //     Time.deltaTime);

                    m_InteractionTransform.up = (Vector2) direction;
                    float distance = Vector2.Distance(transform.position,
                        PathFinding.CurrentPath.vectorPath[PathFinding.CurrentWayPoint]);

                    if (distance < PathFinding.NextWayPointDistance)
                        PathFinding.CurrentWayPoint++;
                }
                if (m_Seeker.IsDone())
                    m_Seeker.StartPath(transform.position, CurrentEnemy.position, OnPathComplete);

                if (CurrentEnemy == null) yield break;

                yield return null;
            }
        }

        public virtual IEnumerator Attack()
        {
            if (!CanAttack) yield break;
            Fire();
            yield return null;
        }

        public virtual IEnumerator Protect()
        {
            StartCoroutine(Hunt());
            yield break;
        }

        public IEnumerator Search()
        {
            while (true)
            {
                List<RaycastHit2D> hitResults =
                    new List<RaycastHit2D>(Physics2D.CircleCastAll((Vector2) transform.position, 10f, Vector2.zero));
                if (hitResults.Count != 0)
                {
                    IsEnemyNearBy = true;
                    IEnumerable<RaycastHit2D> query = hitResults.OrderBy(element =>
                        Vector3.Distance(element.transform.position, transform.position));
                    foreach (var element in query)
                    {
                        if (element.transform.gameObject.GetComponent<Character>() != null &&
                            element.transform.gameObject.GetComponent<Character>() != this)
                        {
                            CurrentEnemy = element.transform;
                            Debug.Log(CurrentEnemy);
                            yield break;
                        }
                    }
                }
                else
                {
                    CurrentEnemy = null;
                    IsEnemyNearBy = false;
                }

                yield return null;
            }
        }

        public void StartStateAction(IEnumerator action)
        {
            StartCoroutine(action);
        }

        public void StopStateAction(IEnumerator action)
        {
            StopCoroutine(action);
        }
    }
}