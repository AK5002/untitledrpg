﻿using System;
using UnityEngine;

namespace Core.Game
{
    public class Stats:ICloneable
    {
        private StatsData StatsData;

        public float Health {get => StatsData.Health; private set => StatsData.Health = value; }
        public float Stamina {get => StatsData.Stamina; private set => StatsData.Stamina = value; }
        public float Mana {get => StatsData.Mana; private set => StatsData.Mana = value; }
        
        public Stats(float health, float stamina, float mana)
        {
            MaxHealth = Health = health;
            MaxStamina = Stamina = stamina;
            MaxMana = Mana = mana;
        }

        private Stats(StatsData data)
        {
            StatsData = data;
        }

        private float MaxHealth {get => StatsData.MaxHealth; set => StatsData.MaxHealth = value; }
        private float MaxStamina {get => StatsData.MaxStamina; set => StatsData.MaxStamina = value; }
        private float MaxMana {get => StatsData.MaxMana; set => StatsData.MaxMana = value; }

        public void IncrementStats(float healthIncrement, float staminaIncrement, float manaIncrement)
        {
            Health = Mathf.Clamp(Health + healthIncrement, 0, MaxHealth);
            Stamina = Mathf.Clamp(Stamina + staminaIncrement, 0, MaxStamina);
            Mana = Mathf.Clamp(Mana + manaIncrement, 0, MaxMana);
        }

        public StatsData Save() => StatsData;
        public object Clone()
        {
            return new Stats(StatsData);
        }
    }

    [Serializable]
    public struct StatsData
    {
        public float MaxHealth;
        public float MaxStamina;
        public float MaxMana;

        public float Health;
        public float Stamina;
        public float Mana;
    }
}