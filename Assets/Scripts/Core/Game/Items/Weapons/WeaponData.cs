﻿using System;
using Core.Game.Weapons;
using UnityEngine;

namespace Core.Game.Items.Weapons
{
    [CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/Items/WeaponData")]
    public class WeaponData : Item
    {
        public ItemID ItemID;
        public int ArrowCost;

        public float HealthDamage;
        public Vector2 HitBoxFinalPos;
        public Vector2 HitBoxFinalScale;

        public Vector2 HitBoxStartPos;

        public Vector2 HitBoxStartScale;
        public float ManaCost;
        public float ManaDamage;
        public float StaminaCost;
        public float StaminaDamage;

        public AbstractWeapon WeaponPrefab;
        public AudioClip FireSound;

        public override ItemID ID => ItemID;
        public override void Use()
        {
            mInventory.OnWeaponChanged(this);
        }
    }
}