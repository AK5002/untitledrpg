﻿using System;
using UnityEngine;

namespace Core.Game.Items
{
    [CreateAssetMenu(fileName = "GoldSack", menuName = "ScriptableObjects/Items/GoldSack")]
    public class GoldSack : Item
    {
        public override void Use()
        {
            
        }

        public override ItemID ID => new ItemID(-1,0);
    }
}