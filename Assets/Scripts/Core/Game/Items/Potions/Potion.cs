﻿using System;
using UnityEngine;

namespace Core.Game.Items.Potions
{
    [CreateAssetMenu(fileName = "Potion", menuName = "ScriptableObjects/Items/Potion")]
    public class Potion : Item
    {
        public ItemID ItemID;
        public int HealthEffect;
        public int ManaEffect;
        public int StaminaEffect;

        public override void Use()
        {
            mInventory.OnPotionDrunk?.Invoke(HealthEffect, StaminaEffect, ManaEffect);
            mInventory.RemoveItem(this);
        }

        public override ItemID ID => ItemID;
    }
}