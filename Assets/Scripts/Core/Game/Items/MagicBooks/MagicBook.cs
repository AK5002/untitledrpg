using Core.Game.Items.Weapons;

namespace Core.Game.Items.MagicBooks
{
    public class MagicBook : Item
    {
        public ItemID m_ItemID;
        public WeaponData AcquiredMagic;
        public override void Use()
        {
            mInventory.OnMagicBookRead(AcquiredMagic);
            mInventory.RemoveItem(this);
        }

        public override ItemID ID => m_ItemID;
    }
}