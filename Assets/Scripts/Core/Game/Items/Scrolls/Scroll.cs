﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Game.Items.Scrolls
{
    [CreateAssetMenu(fileName = "Scroll", menuName = "ScriptableObjects/Items/Scroll")]
    public class Scroll : Item
    {
        public ItemID ItemID;
        public TextAsset ScrollFile;

        public override void Use()
        {
            mInventory.OnScrollOpened(ScrollFile.text);
        }

        public override ItemID ID => ItemID;
    }
}

