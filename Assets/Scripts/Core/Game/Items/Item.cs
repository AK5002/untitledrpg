﻿using System;
using UnityEngine;

namespace Core.Game.Items
{
    public abstract class Item : ScriptableObject
    {
        public int Cost;
        public string Description;

        public Sprite Icon;

        protected Inventory mInventory;
        public string Name;
        public int Weight;
        

        public virtual void Setup(Inventory inventory)
        {
            if(mInventory != null)
                mInventory.RemoveItem(this);
            
            mInventory = inventory;
        }
        
        public abstract void Use();
        public abstract ItemID ID { get; }
    }

    [Serializable]
    public struct ItemID
    {
        public int Type;
        public int SubType;

        public static bool operator ==(ItemID id1, ItemID id2) => id1.Type == id2.Type && id1.SubType == id2.SubType;

        public static bool operator !=(ItemID id1, ItemID id2) => !(id1 == id2);

        public ItemID(int type, int subType)
        {
            Type = type;
            SubType = subType;
        }
    }
}