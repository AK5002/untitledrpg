﻿using DG.Tweening;
using UnityEngine;

namespace Core.Game.Weapons
{
    public class Flame : AbstractWeapon
    {
        public override void Fire()
        {
            if (IsFiring) return;

            IsFiring = true;
            m_HitBox.enabled = true;

            m_AudioSource.Play();
            OnFired?.Invoke(mWeaponData.StaminaCost, mWeaponData.ManaCost, mWeaponData.ArrowCost);
            transform.DOLocalMove(mWeaponData.HitBoxFinalPos, 1);
            transform.DOScale(mWeaponData.HitBoxFinalScale, 1);
        }

        public override void StopFiring()
        {
            transform.DOLocalMove(mWeaponData.HitBoxStartPos, 1);
            transform.DOScale(mWeaponData.HitBoxStartScale, 1);
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetComponent<IApplyDamage>() != null)
                OnWeaponHit(other.gameObject.GetComponent<IApplyDamage>());
        }
        
        private void Update()
        {
            if (transform.localPosition == (Vector3) mWeaponData.HitBoxStartPos)
            {
                IsFiring = false;
                //m_HitBox.enabled = false;
            }
                
        }
    }
}