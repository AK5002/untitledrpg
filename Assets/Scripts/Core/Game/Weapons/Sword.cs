﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Core.Game.Weapons
{
    public class Sword : AbstractWeapon
    {
        public override void Fire()
        {
            if (IsFiring || transform.localPosition != (Vector3) mWeaponData.HitBoxStartPos) return;

            IsFiring = true;
            m_HitBox.enabled = true;
            
            m_AudioSource.Play();
            OnFired?.Invoke(mWeaponData.StaminaCost, mWeaponData.ManaCost, mWeaponData.ArrowCost);
            transform.DOPunchPosition(mWeaponData.HitBoxFinalPos, 0.8f, 2, 5).OnComplete(() => transform.localPosition = mWeaponData.HitBoxStartPos);
        }

        public override void StopFiring()
        {
            
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetComponent<IApplyDamage>() != null)
                OnWeaponHit(other.gameObject.GetComponent<IApplyDamage>());
        }

        private void Update()
        {
            if (transform.localPosition == (Vector3) mWeaponData.HitBoxStartPos)
            {
                IsFiring = false;
            }
                
        }
    }
}