﻿using System;
using Core.Game.Items.Weapons;
using UnityEngine;

namespace Core.Game.Weapons
{
    public class WeaponHolder : MonoBehaviour
    {
        [SerializeField] public WeaponData m_WeaponData;
        private AbstractWeapon ActiveWeapon { get; set; }
        public event Action<float, float, int> OnWeaponUsed;
        public event Action<IApplyDamage,bool> OnWeaponHitCharacter;

        public WeaponState _WeaponState { get; private set; }

        public void Setup()
        {
            if (m_WeaponData != null)
                ChangeWeapon(m_WeaponData);
        }

        public void ChangeWeapon(WeaponData weaponData)
        {
            if (ActiveWeapon != null)
                Destroy(ActiveWeapon.gameObject);

            var weapon = Instantiate(weaponData.WeaponPrefab, transform);
            ActiveWeapon = weapon;
            m_WeaponData = weaponData;
            weapon.Setup(weaponData, (stamina, mana, arrow) => OnWeaponUsed?.Invoke(stamina, mana, arrow),
                (character,dead) => OnWeaponHitCharacter.Invoke(character,dead));
            _WeaponState = WeaponState.Out;
        }

        public void Fire()
        {
            if (ActiveWeapon == null || _WeaponState == WeaponState.BeingUsed) return;

            ActiveWeapon.Fire();
            _WeaponState = WeaponState.BeingUsed;
        }

        public void StopFire()
        {
            if (_WeaponState != WeaponState.BeingUsed) return;
            
            ActiveWeapon.StopFiring();
            _WeaponState = WeaponState.Out;
        }

        private void Update()
        {
            if(ActiveWeapon != null)
                if(!ActiveWeapon.IsFiring)
                    _WeaponState = WeaponState.Out;
        }
    }
    
    

    public enum WeaponState
    {
        Hidden,
        Out,
        BeingUsed
    }
}