﻿using System;
using Core.Game.Items.Weapons;
using UnityEngine;

namespace Core.Game.Weapons
{
    public abstract class AbstractWeapon : MonoBehaviour
    {
        [SerializeField] protected BoxCollider2D m_HitBox;
        [SerializeField] protected AudioSource m_AudioSource;
        
        public bool IsFiring { get;protected set; }
        protected WeaponData mWeaponData;
        protected Action<float, float, int> OnFired;
        protected Action<IApplyDamage,bool> OnHit;

        public void Setup(WeaponData data, Action<float, float, int> onFired, Action<IApplyDamage,bool> onHit)
        {
            mWeaponData = data;

            OnFired = onFired;
            OnHit = onHit;

            m_AudioSource.clip = mWeaponData.FireSound;
            m_HitBox.enabled = false;
        }

        public abstract void Fire();

        public abstract void StopFiring();

        protected void OnWeaponHit(IApplyDamage character)
        {
            OnHit?.Invoke(character,character.TakeDamage(mWeaponData.HealthDamage, mWeaponData.StaminaDamage, mWeaponData.ManaDamage));
        }
        
    }
}