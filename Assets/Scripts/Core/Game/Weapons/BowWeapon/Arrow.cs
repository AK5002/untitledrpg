using System;
using UnityEngine;

namespace Core.Game.Weapons.BowWeapon
{
    public class Arrow : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D m_Rigidbody;
        public Rigidbody2D Rigidbody => m_Rigidbody;
        public event Action<Character> OnWeaponHit;

        private bool IsFlying => m_Rigidbody.velocity != Vector2.zero;

        public void Setup(Action<Character> onWeaponHit)
        {
            OnWeaponHit = onWeaponHit;
        }
        
        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetComponent<Character>() != null && IsFlying)
            {
                OnWeaponHit?.Invoke(other.gameObject.GetComponent<Character>());
                Debug.Log(other.gameObject);
                Destroy(gameObject);
            }
                
        }
    }
}