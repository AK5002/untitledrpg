using System;
using System.Collections;
using Cinemachine.Utility;
using UnityEngine;

namespace Core.Game.Weapons.BowWeapon
{
    
    public class Bow : AbstractWeapon
    {
        [SerializeField] private Arrow m_ArrowPrefab;

        private int ArrowStartVelocity;
        
        public override void Fire()
        {
            if (IsFiring) return;

            IsFiring = true;
            m_AudioSource.Play();
            
            StartCoroutine(DrawBowString());
        }
        
        public override void StopFiring()
        {
            Debug.Log("e");
            IsFiring = false;
            
            OnFired?.Invoke(mWeaponData.StaminaCost, mWeaponData.ManaCost, mWeaponData.ArrowCost);

            StopCoroutine(DrawBowString());
            var arrow = Instantiate(m_ArrowPrefab);
            arrow.Setup(OnWeaponHit);
            arrow.transform.position = transform.position;
            arrow.transform.eulerAngles = transform.eulerAngles;
            float angle = ( 180-  transform.eulerAngles.z ) * Mathf.Deg2Rad;
            arrow.Rigidbody.AddForce(new Vector2( -Mathf.Sin(angle),-Mathf.Cos(angle))* ArrowStartVelocity, ForceMode2D.Impulse);
            ArrowStartVelocity = 0;
            
        }

        private IEnumerator DrawBowString()
        {
            if(ArrowStartVelocity <= 20)
                ArrowStartVelocity += 5;
            
            yield return new WaitForSeconds(1);
        }

        private void Update()
        {
            IsFiring = ArrowStartVelocity != 0;
        }
    }
}