﻿using System;
using System.Collections.Generic;
using Core.Game.Items;
using Core.Game.Items.Weapons;
using Services.DependencyInjection;
using UnityEngine;

namespace Core.Game
{
    public class Inventory : MonoBehaviour, IInjectable
    {
        [SerializeField] private List<ItemContainer> m_StartItems;
        [SerializeField] private int maxWeight;
        [SerializeField] private int m_StartGoldAmount;
        
        public List<Item> AllItems { get; set; }  = new List<Item>();
        public Action<float, float, float> OnPotionDrunk;
        public Action<WeaponData> OnWeaponChanged;
        public Action<string> OnScrollOpened;
        private List<WeaponData> AvailableMagic;

        public int Weight { get; private set; }
        public int MaxWeight => maxWeight;

        public int GoldAmount { get; private set; }
        public int ArrowAmount { get; set; } = 10;

        public bool IsOverweight => Weight > maxWeight;

        public void Setup()
        {
            GoldAmount = m_StartGoldAmount;

            foreach (var container in m_StartItems)
            {
                for (int i = 0; i < container.Count; i++)
                {
                    AddItem(Instantiate(container.Item));
                }
            }
        }

        public void AddItem(UnityEngine.Object itemToAdd)
        {
            var item = (Item) itemToAdd;
            if (item.ID.Type == -1)
            {
                GoldAmount += item.Cost;
                item.Setup(this);
                return;
            }

            item = Instantiate(item);
            AllItems.Add(item);
            item.Setup(this);
            Weight += item.Weight;
        }

        public void RemoveItem(UnityEngine.Object itemToRemove)
        {
            var item = (Item)itemToRemove;
            if (item.ID.Type == -1)
            {
                GoldAmount -= item.Cost;
                return;
            }

            item = AllItems.Find(itemtoFind => itemtoFind.ID == item.ID);
            if (item != null)
            {
                AllItems.Remove(item);
                Weight -= item.Weight;   
            }
        }

        public GoldSack CreateGoldSack(int goldAmount)
        {
            var goldSack = ScriptableObject.CreateInstance<GoldSack>();
            goldSack.Cost = goldAmount;
            GoldAmount -= goldAmount;
            return goldSack;
        }

        public void OnMagicBookRead(WeaponData weaponData)
        {
            AvailableMagic.Add(weaponData);
        }
    }

    [Serializable]
    public struct ItemContainer
    {
        public Item Item;
        public int Count;
    }

    [Serializable]
    public struct InventoryData
    {
        
    }
}