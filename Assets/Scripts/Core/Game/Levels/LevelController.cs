﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Core.Game.Levels
{
    public class LevelController : MonoBehaviour
    {
        [SerializeField] private List<Level> m_Levels;
        public Level CurrentLevel { get; private set; }
        
        public void LoadLevel(int id, LevelData data)
        {
            var level = m_Levels.FirstOrDefault(levelToFind => data == null ? levelToFind.m_Id == id : levelToFind.m_Id == data.Id);
            if (level == null)
            {
                Debug.LogError("Could not Find Level");
                return;
            }
            CurrentLevel = Instantiate(level, transform);
            CurrentLevel.Load(data);
        }

        public List<LevelData> CreateSaveData()
        {
            var levelDatas = new List<LevelData>();

            foreach (var level in m_Levels) levelDatas.Add(level.CreateSaveData());

            return levelDatas;
        }
    }
}