﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Game.Levels.Areas
{
    public class AreaController : MonoBehaviour
    {
        [SerializeField] private List<Area> m_Areas;

        public event Action<Character, List<IFightable>> OnBattleStarted;
        
        public void Load(List<AreaData> datas)
        {
            foreach (var area in m_Areas)
            {
                var data = datas?.Find(dataToFind => dataToFind.Id == area.m_Id);
                area.Load(data);
            }

            if (datas == null) return;
        }

        public List<AreaData> CreateSaveData()
        {
            var areaDatas = new List<AreaData>();
            
            foreach (var area in m_Areas) areaDatas.Add(area.CreateSaveData());

            return areaDatas;
        }
    }
}