﻿using System;
using System.Collections.Generic;
using Core.Game.Interactables;
using Core.Game.NPC;
using Core.Game.Relations;
using UnityEngine;

namespace Core.Game.Levels.Areas
{
    public class Area : MonoBehaviour
    {
        [SerializeField] public int m_Id;
        [SerializeField] private List<AbstractInteractable> m_Interactables;
        [SerializeField] private List<NonPlayableCharacter> m_NPCs;

        private List<AbstractInteractable> DestroyedInteractables;
        

        public void Load(AreaData data)
        {
            foreach (var npc in m_NPCs)
            {
                npc.Setup(data?.m_NPCs.Find(dataToFind => dataToFind.Id == npc.Id));
                npc.OnNPCDead += OnNpcDeadHandler;
            }
            
            foreach (var interactable in m_Interactables) interactable.Setup();
            
            foreach (var interactable in m_Interactables)
                interactable.OnInteractableDestroyed += OnInteractableDestroyedHandler;

            DestroyedInteractables = data != null ? data.DestroyedInteractables : new List<AbstractInteractable>();

            foreach (var interactable in DestroyedInteractables)
            {
                m_Interactables.Remove(interactable);
                DestroyedInteractables.Remove(interactable);
                Destroy(interactable.gameObject);
            }

            foreach (var npc in m_NPCs)
            {
                if (npc is IProtectionSubject)
                {
                    foreach (var guard in m_NPCs)
                    {
                        if(guard is IProtectionObserver && guard.Faction == npc.Faction)
                            (npc as IProtectionSubject).Attach(guard as IProtectionObserver);
                    }
                }
            }
        }

        private void OnNpcDeadHandler(NonPlayableCharacter npc)
        {
            m_Interactables.Add(npc.InventoryInteractable);
            npc.InventoryInteractable.Setup();
            m_NPCs.Remove(npc);
            Destroy(npc);
        }


        public void OnCharacterAreaEnter(Character character)
        {

            foreach (var npc in m_NPCs)
            {
                if (npc is IFightable && Relationship.Instance.GetRelation(character.Faction, npc.Faction) == RelationType.Hostile)
                    (npc as IFightable).SearchForEnemy = true;
            }
            
        }

        public void OnCharacterAreaExit(Transform character)
        {
        }

        public AreaData CreateSaveData()
        {
            return new AreaData(m_Id, m_NPCs, m_Interactables, DestroyedInteractables);
        }

        private void OnInteractableDestroyedHandler(AbstractInteractable interactable)
        {
            DestroyedInteractables.Add(interactable);
            m_Interactables.Remove(interactable);
        }
    }
    
    public class AreaData
    {
        public readonly List<AbstractInteractable> DestroyedInteractables;
        public readonly int Id;
        public readonly List<AbstractInteractable> Interactables;
        public readonly List<NPC.NonPlayableCharacter> m_NPCs;

        public AreaData(int id, List<NPC.NonPlayableCharacter> mNpCs, List<AbstractInteractable> interactables,
            List<AbstractInteractable> destroyedInteractables)
        {
            Id = id;
            m_NPCs = mNpCs;
            Interactables = interactables;
            DestroyedInteractables = destroyedInteractables;
        }
    }
}