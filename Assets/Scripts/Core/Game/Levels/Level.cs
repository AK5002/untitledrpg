﻿using System;
using System.Collections.Generic;
using Core.Game.Levels.Areas;
using Core.Game.PlayerCharacter;
using UnityEngine;

namespace Core.Game.Levels
{
    public class Level : MonoBehaviour
    {
        [SerializeField] public int m_Id;
        [SerializeField] public AreaController m_AreaController;

        public void Load(LevelData data)
        {
            Enter();
            m_AreaController.Load(data?.AreaDatas);
        }

        private void Enter()
        {
            gameObject.SetActive(true);
        }

        private void Exit()
        {
            gameObject.SetActive(false);
        }

        public LevelData CreateSaveData()
        {
            var data = new LevelData(m_Id, m_AreaController.CreateSaveData(), m_AreaController);
            return data;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if(other.gameObject.GetComponent<Player>() != null)
                Exit();
        }
    }
    
    public class LevelData
    {
        public readonly AreaController AreaController;
        public readonly List<AreaData> AreaDatas;
        public readonly int Id;

        public LevelData(int id, List<AreaData> areaDatas, AreaController areaController)
        {
            Id = id;
            AreaDatas = areaDatas;
            AreaController = areaController;
        }
    }
}