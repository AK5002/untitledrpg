﻿using Core.Game;
using Services.DependencyInjection;
using UnityEngine;

namespace Core.Managers
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GamePlayController m_GamePlayController;
        [SerializeField] private AstarPath m_Pathfinder;
        [SerializeField] private Injector m_Injector;
        
        private void Awake()
        {
            Application.targetFrameRate = 60;
            m_Injector.Setup();
            m_GamePlayController.Setup();
            m_GamePlayController.OnNewLevelLoaded += () => m_Pathfinder.Scan();
        }
    }
}