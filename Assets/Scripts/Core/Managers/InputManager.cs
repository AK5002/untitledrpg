﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.Managers
{
    public class InputManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler,IPlayerInput
    {
        [Header("Input Buttons")] [SerializeField]
        private FixedJoystick m_Joystick;
        [SerializeField] private GameObject m_FireButton;

        [SerializeField] public Button m_UseButton;
        [SerializeField] public Text m_UseButtonText;
        
        public event Action<Vector3> OnPlayerMoveByDirection;
        public event Action OnUseButtonPressed;
        public event Action OnFireButtonDown;
        public event Action OnFireButtonUp;

        private bool OnFireButton;
        private bool OnFireButtonPressed;

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (eventData.pointerEnter.gameObject == m_FireButton)
                OnFireButton = true;
        }
        // public void OnPointerUp(PointerEventData eventData)
        // {
        //     Debug.Log("a");
        //     if (eventData.pointerEnter.gameObject == m_FireButton)
        //         OnFireButtonUp?.Invoke();
        // }
        
        public void OnPointerExit(PointerEventData eventData)
        {
            if (eventData.pointerEnter.gameObject == m_FireButton)
                OnFireButton = false;
        }

        public void Setup()
        {
            m_UseButton.onClick.RemoveAllListeners();

            m_UseButton.onClick.AddListener(() => OnUseButtonPressed?.Invoke());
        }

        private void Update()
        {
            
            if (m_Joystick.Direction != Vector2.zero)
                OnPlayerMoveByDirection?.Invoke(m_Joystick.Direction);
            
#if UNITY_EDITOR
            if (OnFireButton && Input.GetMouseButton(0))
            {
                OnFireButtonDown?.Invoke();
                OnFireButtonPressed = true;
            }
            else
            {
                if(OnFireButtonPressed && OnFireButton)
                    OnFireButtonUp?.Invoke();

                OnFireButtonPressed = false;
            }
                 
#endif
#if UNITY_ANDROID
            if(Input.touchCount >0)
                if(OnFireButton && Input.GetTouch(0).phase == TouchPhase.Began)
                    OnFireButtonDown?.Invoke();    
#endif
#if UNITY_IOS 
            if(Input.touchCount >0)
                if(OnFireButton && Input.GetTouch(0).phase == TouchPhase.Began)
                    OnFireButtonDown?.Invoke();  
#endif

        }

    }

    public interface IPlayerInput
    {
        event Action<Vector3> OnPlayerMoveByDirection;
        event Action OnUseButtonPressed;
        event Action OnFireButtonDown;
        event Action OnFireButtonUp;
    }
}