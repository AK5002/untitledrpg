﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PuzzleSystem
{
    public class PuzzleController:MonoBehaviour
    {
        [SerializeField] private Transform m_TextPuzzleContainer;
        [SerializeField] private Transform m_ColorPuzzleContainer;
        
        private PuzzleHolder CurrentHolder;
        private AbstractPuzzle CurrentPuzzle;

        public void BeginPuzzle(PuzzleHolder holder)
        {
            if (holder == null || CurrentHolder != null) return;
            
            CurrentHolder = holder;

            var container = m_TextPuzzleContainer;

            if (holder.Puzzle.PuzzleType == PuzzleType.Color)
                container = m_ColorPuzzleContainer;
            
            CurrentPuzzle = Instantiate(CurrentHolder.Puzzle,container);
            CurrentPuzzle.OnComplete = OnCompleteHandler;
        }

        private void OnCompleteHandler()
        {
            if (CurrentHolder == null) return;
         
            Debug.Log("ok");
            CurrentHolder.Subject.OnPuzzleSolved();
            
            Destroy(CurrentPuzzle.gameObject);
            CurrentPuzzle = null;
            CurrentHolder = null;
        }
    }

}
