using System;
using UnityEngine;

namespace PuzzleSystem
{
    public abstract class AbstractPuzzle : MonoBehaviour
    {
        [SerializeField] protected PuzzleType m_Type;

        public PuzzleType PuzzleType => m_Type;
        public abstract Action OnComplete { protected get; set; }
    }
    
    public enum PuzzleType
    {
        Text,
        Color,
    }
}