using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PuzzleSystem.ColorPuzzleComponents
{
    public class ColorPuzzle : AbstractPuzzle,IDragHandler,IDropHandler, IPointerDownHandler
    {
        [SerializeField] private List<ColorPuzzlePiece> m_AllPieces;
        [SerializeField] private int m_PuzzleCount;
        
        private List<ColorPuzzlePiece> ActivePieces = new List<ColorPuzzlePiece>();
        private List<ColorPuzzlePiece> PassivePieces = new List<ColorPuzzlePiece>();

        public override Action OnComplete { protected get; set; }

        private DragDropController DragDropController = new DragDropController();
        private Selector Selector = new Selector();

        private int placedItems;

        private void Awake()
        {
            foreach (var piece in m_AllPieces)
            {
                if(piece.IsActive)
                    ActivePieces.Add(piece);
                else PassivePieces.Add(piece);
            }

            DragDropController.OnDropped += OnDroppedHandler;
            Selector.OnSelect += (piece) => DragDropController.CurrentPiece = piece;
        }

        public void OnDrag(PointerEventData eventData)
        {
            DragDropController.OnDrag(eventData);
        }

        public void OnDrop(PointerEventData eventData)
        {
           DragDropController.OnDrop();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Selector.OnObjectSelected(eventData.pointerCurrentRaycast.gameObject);
        }
        
        private void OnDroppedHandler(ColorPuzzlePiece piece)
        {
            var passivePiece = PassivePieces.Find(pieceToFind => pieceToFind.Color == piece.Color);

            if (!IsOverlap(piece.m_RectTransform, passivePiece.m_RectTransform)) return;
            
           piece.m_RectTransform.anchoredPosition = passivePiece.m_RectTransform.anchoredPosition;
           piece.IsActive = false;

           placedItems++;
           if(placedItems == m_PuzzleCount) OnComplete?.Invoke();
        }
        
        private bool IsOverlap(RectTransform rectTransform1, RectTransform rectTransform2)
        {
            Rect rect1 = new Rect(rectTransform1.localPosition.x,rectTransform1.localPosition.y,rectTransform1.rect.width,rectTransform1.rect.height);
            Rect rect2 = new Rect(rectTransform2.localPosition.x,rectTransform2.localPosition.y,rectTransform2.rect.width,rectTransform2.rect.height);

            return rect1.Overlaps(rect2);
        }
    }
}