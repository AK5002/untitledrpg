using UnityEngine;

namespace PuzzleSystem.ColorPuzzleComponents
{
    public class ColorPuzzlePiece : MonoBehaviour
    {
        [SerializeField] public RectTransform m_RectTransform;
        [SerializeField] private PuzzleColor m_Color;
        
        public PuzzleColor Color => m_Color;
        public bool IsActive;
    }
    
    public enum PuzzleColor
    {
        Red,
        Green,
        Blue,
    }
}