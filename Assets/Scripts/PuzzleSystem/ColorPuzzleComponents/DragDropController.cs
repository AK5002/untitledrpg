using System;
using UnityEngine.EventSystems;
using UnityEngine;

namespace PuzzleSystem.ColorPuzzleComponents
{
    public class DragDropController
    {
        public event Action<ColorPuzzlePiece> OnDropped;
        
        public ColorPuzzlePiece CurrentPiece;
        
        public void OnDrag(PointerEventData data)
        {
            if (CurrentPiece != null && CurrentPiece.IsActive &&
                data.pointerCurrentRaycast.screenPosition != Vector2.zero)
                CurrentPiece.transform.position = data.pointerCurrentRaycast.screenPosition;
        }

        public void OnDrop()
        {
            if(CurrentPiece != null && CurrentPiece.IsActive)
                OnDropped?.Invoke(CurrentPiece);
        }
    }
    
    public class Selector
    {
        public event Action<ColorPuzzlePiece> OnSelect; 
    
        public void OnObjectSelected(GameObject gameObject)
        {
            ColorPuzzlePiece selectedObject = gameObject.GetComponent<ColorPuzzlePiece>();

            if (selectedObject != null)
                OnSelect?.Invoke(selectedObject);
        }
    }
}