using UnityEngine;

namespace PuzzleSystem
{
    public class PuzzleHolder : MonoBehaviour
    {
        [SerializeField] private AbstractPuzzle m_Puzzle;
        [SerializeField] private PuzzleSubject m_Subject;

        public AbstractPuzzle Puzzle => m_Puzzle;
        public PuzzleSubject Subject => m_Subject;
    }
}