using UnityEngine;

namespace PuzzleSystem
{
    public class PuzzleSubject : MonoBehaviour
    {
        [SerializeField] private Vector3 m_MoveBy;
        
        public void OnPuzzleSolved()
        {
            transform.position += m_MoveBy;
        }
    }
}