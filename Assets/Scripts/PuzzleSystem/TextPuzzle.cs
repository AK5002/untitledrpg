using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

namespace PuzzleSystem
{
    public class TextPuzzle : AbstractPuzzle 
    {
        [Header("PuzzleComponents")]
        [SerializeField] private string m_QuestionText;
        [SerializeField] private string m_RightAnswer;
        
        [Header("References")]
        [SerializeField] private Text m_Question;
        [SerializeField] private InputField m_AnswerField;
        [SerializeField] private Button m_AnswerButton;

        private string GivenAnswer;

        public override Action OnComplete { protected get; set; }

        private void Awake()
        {
            m_Question.text = m_QuestionText;
            
            m_AnswerButton.onClick.RemoveAllListeners();
            m_AnswerButton.onClick.AddListener(TryAnswer);
        }

        private void TryAnswer()
        {
            GivenAnswer = m_AnswerField.text;
            if(GivenAnswer != null && ProcessedAnswer(GivenAnswer) == m_RightAnswer) OnComplete?.Invoke();
        }

        private string ProcessedAnswer(string rawAnswer)
        {
            List<char> answer = new List<char>(rawAnswer.ToCharArray());
            if (answer[0] == ' ')
                answer.RemoveAt(0);
            if (answer.Last() == ' ')
                answer.Remove(answer.Last());

            return new string(answer.ToArray());
        }
    }
}