﻿using System.Collections.Generic;
using Core.Game;
using QuestSystem;

namespace UI
{
    public abstract class UIData
    {
    }
    
    public class GamePlayViewData : UIData
    {
        public readonly Stats PlayerStats;

        public GamePlayViewData(Stats playerStats)
        {
            PlayerStats = playerStats;
        }
    }
    public class InventoryPopUpData : UIData
    {
        public readonly Inventory PlayerInventory;
        public readonly Inventory OtherInventory;

        public InventoryPopUpData(Inventory playerInventory,Inventory otherInventory)
        {
            PlayerInventory = playerInventory;
            OtherInventory = otherInventory;
        }
    }
    public class ScrollPopUpData : UIData
    {
        public readonly string Content;

        public ScrollPopUpData(string content)
        {
            Content = content;
        }
    }

    public class QuestPopUpData : UIData
    {
        public readonly QuestData MainQuestData;
        public readonly List<QuestData> SideQuestsDatas;

        public QuestPopUpData(QuestData mainQuestData, List<QuestData> sideQuestsDatas)
        {
            MainQuestData = mainQuestData;
            SideQuestsDatas = sideQuestsDatas;
        }
    }
}