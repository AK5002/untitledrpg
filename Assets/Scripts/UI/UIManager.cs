﻿using System;
using System.Collections.Generic;
using Core.Game.Items;
using UI.PopUps;
using UI.Views;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] public Transform m_PopUpContainer;
        [SerializeField] public List<PopUpMenu> m_PopUpPrefabs;
        [SerializeField] public Transform m_ViewContainer;
        [SerializeField] public List<ViewMenu> m_ViewPrefabs;
        public UnityAction OnInventoryButton;
        public Action<Item> OnInventoryLootCell;
        public Action OnDialogueClosed;
        public UnityAction OnPauseButton;
        public Action<GoldSack> OnLootGoldButton;
        public Action<int> OnDirectionChosen;
        public Action<Item,bool> OnStoreCellHandler;
        
        [HideInInspector] public PopUpController PopUpController;
        [HideInInspector] public ViewController ViewController;

        public void Setup()
        {
            if (PopUpController == null)
                PopUpController = new PopUpController(this);
            if (ViewController == null)
                ViewController = new ViewController(this);
        }
    }
}