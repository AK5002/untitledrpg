﻿using UI.Views;
using UnityEngine;

namespace UI
{
    public class ViewController : AbstractPresenter
    {
        public ViewController(UIManager uiManager)
        {
            UiManager = uiManager;
        }

        private ViewMenu CurrentView { get; set; }

        public T Show<T, K>(K data) where T : ViewMenu where K : UIData
        {
            if (CurrentView != null)
            {
                Hide(CurrentView);
                if (UiManager.PopUpController.CurrentPopUp != null)
                    UiManager.PopUpController.Hide(UiManager.PopUpController.CurrentPopUp);
            }

            var menu = UiManager.m_ViewPrefabs.Find(view => view.Group == typeof(T));

            menu = Object.Instantiate(menu, UiManager.m_ViewContainer);

            menu.Setup(data, this);
            menu.OnShow();

            CurrentView = menu;

            return (T) menu;
        }

        public void Hide<T>(T menuToHide) where T : ViewMenu
        {
            menuToHide.OnHide();
        }
    }
}