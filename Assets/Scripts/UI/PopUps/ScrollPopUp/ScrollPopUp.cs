﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.ScrollPopUp
{
    public class ScrollPopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;
        [SerializeField] private TMP_Text m_Content;

        private ScrollPopUpData Data;
        public override Type Group => typeof(ScrollPopUp);
        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);
            Data = (ScrollPopUpData) data;
        }

        public override void OnShow()
        {
            m_Content.text = Data.Content;
            m_ExitButton.onClick.RemoveAllListeners();
            m_ExitButton.onClick.AddListener(() => PopUpController.Hide(this));
        }
    }

}
