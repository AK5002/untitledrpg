using QuestSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.QuestMenu
{
    public class QuestCell : MonoBehaviour
    {
        [SerializeField] private RectTransform m_RectTransform;
        [SerializeField] private Text m_NameText;
        [SerializeField] private Text m_DescriptionText;
        [SerializeField] private Toggle m_Toggle;

        public RectTransform RectTransform => m_RectTransform;

        public void Setup(QuestData data)
        {
            m_NameText.text = data.Name;
            m_DescriptionText.text = data.Description;
            m_Toggle.isOn = data.IsRewardAvailable;
        }
    }
}