using System;
using System.Collections.Generic;
using QuestSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.QuestMenu
{
    public class QuestPopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;
        [SerializeField] private QuestCell m_MainQuestCell;
        [SerializeField] private QuestCell m_CellPrefab;
        [SerializeField] private Transform m_SideQuestParent;
        public override Type Group => typeof(QuestPopUp);

        private QuestPopUpData Data;
        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);
            Data = (QuestPopUpData) data;
        }

        public override void OnShow()
        {
            m_ExitButton.onClick.RemoveAllListeners();
            m_ExitButton.onClick.AddListener(() => PopUpController.Hide(this));

            m_MainQuestCell.Setup(Data.MainQuestData);
            foreach (var data in Data.SideQuestsDatas)
            {
                CreateQuestCell(data);
            }
        }

        private void CreateQuestCell(QuestData data)
        {
           var cell = Instantiate(m_CellPrefab, m_SideQuestParent);
           cell.Setup(data);
        }
        
    }
}