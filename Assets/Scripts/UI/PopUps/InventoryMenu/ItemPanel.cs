using System;
using Core.Game.Items;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.InventoryMenu
{
    public class ItemPanel : MonoBehaviour
    {
        [SerializeField] private Image m_Image;
        [SerializeField] private Text m_ItemName;
        [SerializeField] private Text m_Description;
        [SerializeField] private Text m_Cost;
        [SerializeField] private Button m_UseButton;
        [SerializeField] private Text m_ButtonText;

        public void Setup(Item item,InventoryButtonStrategy strategy)
        {
            if(!gameObject.activeSelf) gameObject.SetActive(true);
            m_Image.sprite = item.Icon;
            m_ItemName.text = item.Name;
            m_Description.text = item.Description;
            m_Cost.text = $"Cost: {item.Cost}";
            m_UseButton.onClick.RemoveAllListeners();
            m_UseButton.onClick.AddListener(() => strategy.DoAlgorithm(item));
            m_ButtonText.text = strategy.Action;
        }

        private void Update()
        {
        }
    }
    
      public abstract class InventoryButtonStrategy
    {
        protected readonly IInventoryPopUp InventoryPopUp;
        public abstract string Action { get; }
        
        public InventoryButtonStrategy(IInventoryPopUp inventoryPopUp)
        {
            InventoryPopUp = inventoryPopUp;
        }

        public abstract void DoAlgorithm(Item item);
    }

    public class PlayerInventoryStrategy : InventoryButtonStrategy
    {
        public PlayerInventoryStrategy(IInventoryPopUp inventoryPopUp) : base(inventoryPopUp)
        {
        }

        public override string Action => "Use";

        public override void DoAlgorithm(Item item)
        {
            item.Use();
            InventoryPopUp.UpdateInventory();
        }
    }

    public class LootInventoryStrategy : InventoryButtonStrategy
    {
        private Action<Item> OnLootCell;
        public override string Action => "Loot";

        public LootInventoryStrategy(IInventoryPopUp inventoryPopUp, Action<Item> onLootCell) : base(inventoryPopUp)
        {
            OnLootCell = onLootCell;
        }

        public override void DoAlgorithm(Item item)
        {
            OnLootCell?.Invoke(item);
            InventoryPopUp.UpdateInventory();
        }
    }

    public class StorePlayerInventoryStrategy : InventoryButtonStrategy
    {
        private Action<Item> OnStorePlayerCell;
        
        public StorePlayerInventoryStrategy(IInventoryPopUp inventoryPopUp, Action<Item> onStorePlayerCell) : base(inventoryPopUp)
        {
            OnStorePlayerCell = onStorePlayerCell;
        }

        public override string Action => "Sell";

        public override void DoAlgorithm(Item item)
        {
            OnStorePlayerCell?.Invoke(item);
            InventoryPopUp.UpdateInventory();
        }
    }

    public class StoreInventoryStrategy : InventoryButtonStrategy
    {
        private Action<Item> OnStoreCell;
        public StoreInventoryStrategy(IInventoryPopUp inventoryPopUp, Action<Item> onStoreCell) : base(inventoryPopUp)
        {
            OnStoreCell = onStoreCell;
        }

        public override string Action => "Buy";

        public override void DoAlgorithm(Item item)
        {
            OnStoreCell?.Invoke(item);
            InventoryPopUp.UpdateInventory();
        }
    }
}