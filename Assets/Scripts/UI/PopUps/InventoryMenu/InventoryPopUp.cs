using System;
using System.Collections.Generic;
using Core.Game.Items;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.InventoryMenu
{
    public class InventoryPopUp : PopUpMenu, IInventoryPopUp
    {
        [SerializeField] protected Button m_ExitButton;
        [SerializeField] protected Text m_GoldAmount;
        [SerializeField] private InventoryCell m_CellPrefab;
        [SerializeField] protected ItemPanel m_ItemPanel;
        [SerializeField] protected Transform m_OwnCellParent;

        protected List<InventoryCell> Cells = new List<InventoryCell>();

        public override Type Group => typeof(InventoryPopUp);

        protected InventoryPopUpData Data;

        public override void Setup(UIData data, PopUpController popUpController)
        {
            base.Setup(data, popUpController);
            Data = (InventoryPopUpData) data;
        }

        public override void OnShow()
        {
            m_ExitButton.onClick.RemoveAllListeners();
            m_ExitButton.onClick.AddListener(() => PopUpController.Hide(this));
            m_ItemPanel.gameObject.SetActive(false);
        }

        public virtual void UpdateInventory()
        {
            m_ItemPanel.gameObject.SetActive(false);
            foreach (var cell in Cells)
            {
                cell.Clear();
                Destroy(cell.gameObject);
            }

            Cells = new List<InventoryCell>();

           DispenseItemsToCells(Data.PlayerInventory.AllItems,m_OwnCellParent,Cells);
        }

        protected void DispenseItemsToCells(List<Item> items, Transform parentPanel, List<InventoryCell> cellContainer) // offset - x = 300, y = -300; spacing - x = 200, y = 200
        {
            foreach (var item in items)
            {
                var cell = cellContainer.Find(inventoryCell => inventoryCell.ContainingItem.ID == item.ID);
                if (cell != null) cell.AddItem(item);
                else
                {
                    cell = Instantiate(m_CellPrefab, parentPanel);
                    cell.AddItem(item);
                    cellContainer.Add(cell);
                }
            }
            
        }

        protected virtual void OnCellSelectedHandler(Item cellItem, InventoryCell selectedCell, bool storeCell = false)
        {
            foreach (var cell in Cells)
            {
                if(cell != selectedCell) cell.Deselect();
            }
        }
    }

    public interface IInventoryPopUp
    {
        void UpdateInventory();
    }
}