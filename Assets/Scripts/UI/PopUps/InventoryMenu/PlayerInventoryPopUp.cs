﻿using System;
using Core.Game.Items;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.InventoryMenu
{
    public class PlayerInventoryPopUp : InventoryPopUp
    {
        [SerializeField] private Text m_Weight;
        
        public override Type Group => typeof(PlayerInventoryPopUp);

        public override void OnShow()
        {
            base.OnShow();
            UpdateInventory();
        }

        public override void UpdateInventory()
        {
            base.UpdateInventory();

            m_GoldAmount.text = $"Gold: {Data.PlayerInventory.GoldAmount}";
            m_Weight.text = $"Weight: {Data.PlayerInventory.Weight}/{Data.PlayerInventory.MaxWeight}";

            foreach (var cell in Cells)
            {
                cell.Setup(OnCellSelectedHandler);
            }
                
        }

        protected override void OnCellSelectedHandler(Item cellItem, InventoryCell selectedCell, bool storeCell = false)
        {
            base.OnCellSelectedHandler(cellItem, selectedCell);
            m_ItemPanel.Setup(cellItem, new PlayerInventoryStrategy(this));
        }
    }

}