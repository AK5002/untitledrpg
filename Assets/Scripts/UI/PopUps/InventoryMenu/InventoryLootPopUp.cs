﻿using System;
using Core.Game.Items;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.InventoryMenu
{
    public class InventoryLootPopUp : InventoryPopUp
    {
        [SerializeField] private Button m_LootGoldButton;

        public override Type Group => typeof(InventoryLootPopUp);

        public override void OnShow()
        {
            base.OnShow();
            m_LootGoldButton.onClick.RemoveAllListeners();

            m_LootGoldButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.OnLootGoldButton?.Invoke(
                    Data.OtherInventory.CreateGoldSack(Data.OtherInventory.GoldAmount));
                UpdateInventory();
            });
            
            UpdateInventory();
        }


        private void UpdateInventory()
        {
            m_GoldAmount.text = $"Gold: {Data.OtherInventory.GoldAmount}";
            foreach (var cell in Cells)
            {
                cell.Clear();
                Destroy(cell.gameObject);
            }

            DispenseItemsToCells(Data.OtherInventory.AllItems,m_OwnCellParent,Cells);

            foreach (var cell in Cells)
            {
                cell.Setup(OnCellSelectedHandler);
            }
        }

        protected override void OnCellSelectedHandler(Item cellItem, InventoryCell selectedCell,bool storeCell = false)
        {
            base.OnCellSelectedHandler(cellItem, selectedCell);
            m_ItemPanel.Setup(cellItem,new LootInventoryStrategy(this,PopUpController.UiManager.OnInventoryLootCell));
        }
    }
}