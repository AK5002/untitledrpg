using System;
using System.Collections.Generic;
using Core.Game.Items;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.InventoryMenu
{
    public class StorePopUp : InventoryPopUp
    {
        [SerializeField] private Text m_StoreGoldAmount;
        [SerializeField] private Text StoreSpecialty;
        [SerializeField] private Transform m_StoreItemCellsPanel;
        
        private List<InventoryCell> StoreItemCells = new List<InventoryCell>();

        public override Type Group => typeof(StorePopUp);

        public override void OnShow()
        {
            base.OnShow();
            UpdateInventory();
        }

        public override void UpdateInventory()
        {
            foreach (var cell in Cells)
            {
                cell.Clear();
                Destroy(cell.gameObject);
            }
            
            foreach (var cell in StoreItemCells)
            {
                cell.Clear();
                Destroy(cell.gameObject);
            }

            Cells = new List<InventoryCell>();
            StoreItemCells = new List<InventoryCell>();

            DispenseItemsToCells(Data.PlayerInventory.AllItems,m_OwnCellParent,Cells);
            DispenseItemsToCells(Data.OtherInventory.AllItems, m_StoreItemCellsPanel,StoreItemCells);
            
            m_GoldAmount.text = $"Gold: {Data.PlayerInventory.GoldAmount}";
            m_StoreGoldAmount.text = $"Store Gold: {Data.OtherInventory.GoldAmount}";;

            foreach (var cell in Cells)
            {
                cell.Setup(OnCellSelectedHandler);
            }

            foreach (var storeCell in StoreItemCells)
            {
                storeCell.Setup(OnCellSelectedHandler,true);
            }
        }

        protected override void OnCellSelectedHandler(Item cellItem, InventoryCell selectedCell,bool storeCell = false)
        {
            base.OnCellSelectedHandler(cellItem, selectedCell);
            
            foreach (var cell in StoreItemCells)
            {
                if(cell != selectedCell) cell.Deselect();
            }
            
            if(storeCell)
                m_ItemPanel.Setup(cellItem,new StoreInventoryStrategy(this,(item) => PopUpController.UiManager.OnStoreCellHandler(item,true)));
            else
            {
                m_ItemPanel.Setup(cellItem,new StorePlayerInventoryStrategy(this,(item) => PopUpController.UiManager.OnStoreCellHandler(item,false)));
            }
        }
    }
}