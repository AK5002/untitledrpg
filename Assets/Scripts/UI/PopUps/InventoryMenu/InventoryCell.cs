﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Items;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.InventoryMenu
{
    public class InventoryCell : MonoBehaviour
    {
        [SerializeField] private RectTransform m_RectTransform;
        [SerializeField] private Text CountText;
        [SerializeField] private Image m_Image;
        [SerializeField] private GameObject m_SelectedImage;
        [SerializeField] private Button m_UseButton;

        private List<Item> Items { get; set; } = new List<Item>();
        public Item ContainingItem { get; private set; }
        public RectTransform RectTransform => m_RectTransform;

        private bool _IsSelected;
        private bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                m_SelectedImage.SetActive(_IsSelected);
            }
        }

        public void Setup(Action<Item,InventoryCell,bool> OnSelect, bool isStoreCell = false)
        {
            m_SelectedImage.SetActive(false);
            m_UseButton.onClick.RemoveAllListeners();

            m_UseButton.onClick.AddListener(() =>
            {
                if (!IsSelected)
                {
                    IsSelected = true;
                    if(ContainingItem != null)
                        OnSelect.Invoke(Items.Last(),this,isStoreCell);
                }
            });
            
            CountText.text = $"{Items.Count}";
        }

        public void AddItem(Item item)
        {
            if (ContainingItem == null)
            {
                ContainingItem = item;
                m_Image.sprite = item.Icon;
            }
            
            Items.Add(item);
        }

        public void UpdateCell()
        {
            if (Items.Count == 0)
            {
                m_Image.gameObject.SetActive(false);
                Deselect();
            }

        }
        
        public void Deselect()
        {
            IsSelected = false;
        }

        public void Clear() => Items = new List<Item>();
    }
}