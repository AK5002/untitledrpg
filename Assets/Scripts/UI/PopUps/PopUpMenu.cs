﻿using System;
using UnityEngine;

namespace UI.PopUps
{
    public abstract class PopUpMenu : MonoBehaviour
    {
        protected PopUpController PopUpController { get; set; }


        public abstract Type Group { get; }

        public virtual void Setup(UIData data, PopUpController popUpController)
        {
            PopUpController = popUpController;
        }

        public abstract void OnShow();


        public virtual void OnHide()
        {
            Destroy(gameObject);
        }
    }
}