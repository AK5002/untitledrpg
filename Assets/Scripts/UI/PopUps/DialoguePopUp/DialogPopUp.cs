using System;
using System.Collections.Generic;
using DialogueSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.DialoguePopUp
{
    public class DialogPopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;
        [SerializeField] private Text m_NPCResponse;
        [SerializeField] private ResponseButton m_ResponseButtonPrefab;
        [SerializeField] private TradeConfirmationMenu m_ConfirmationMenuPrefab;
        [SerializeField] private Transform m_ResponsesParent;
        public override Type Group => typeof(DialogPopUp);
        private List<ResponseButton> ResponseButtons = new List<ResponseButton>();

        public override void OnShow()
        {
           
            m_ExitButton.onClick.RemoveAllListeners();
            m_ExitButton.onClick.AddListener(() =>
            {
                PopUpController.Hide(this);
                PopUpController.UiManager.OnDialogueClosed();
            }); 
        }

        public void UpdateMenu(string NPCResponse,ResponseType type, List<DialogueMenuResponse> newResponses)
        {
            foreach (var response in ResponseButtons)
            {
                Destroy(response.gameObject);
            }
            
            ResponseButtons = new List<ResponseButton>();
            m_NPCResponse.text = NPCResponse;
            if (type != null)
            {
                m_NPCResponse.text += type is ITradeResponse ? $" (Received items: {type.Hint})" : $"({type.Hint})";
            }

            foreach (var response in newResponses)
            {
                var responseButton = Instantiate(m_ResponseButtonPrefab, m_ResponsesParent);
                string text = response.Value;
                switch (response.Manner)
                {
                    case ResponseManner.Generous:
                        text += " (Improved Relations)";
                        break;
                    case ResponseManner.Rude:
                        text += " (Worsened Relations)";
                        break;
                    default:
                        text += "";
                        break;
                }
                
                if(response.Type != null)
                    text += response.Type is ITradeResponse? $" (Lose items: {response.Type.Hint})" : $"{response.Type.Hint}";
                responseButton.ResponseText.text = text;
                responseButton.OrderID = response.OrderID;
                responseButton.onClick.RemoveAllListeners();
                responseButton.onClick.AddListener(() =>
                {
                    if (response.Type is ITradeResponse)
                    {
                        var menu = Instantiate(m_ConfirmationMenuPrefab, transform);
                        menu.OnDecisionMade +=  (yes) =>
                        {
                            if(yes)
                                PopUpController.UiManager.OnDirectionChosen(responseButton.OrderID);
                            else Destroy(menu.gameObject);
                        };
                    }
                    else PopUpController.UiManager.OnDirectionChosen(responseButton.OrderID);
                });
                ResponseButtons.Add(responseButton);
            }
            
        }
    }
}