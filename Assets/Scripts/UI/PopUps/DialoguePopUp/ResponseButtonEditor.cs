﻿using UnityEditor;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.DialoguePopUp
{
    [CustomEditor(typeof(ResponseButton))]
    public class ResponseButtonEditor : ButtonEditor
    {
        public override void OnInspectorGUI()
        {
            ResponseButton targetButton = (ResponseButton)target;

            targetButton.RectTransform = (RectTransform) EditorGUILayout.ObjectField("Rect Transform", targetButton.RectTransform, typeof(RectTransform));
            targetButton.ResponseText = (Text)EditorGUILayout.ObjectField("Response Text",targetButton.ResponseText,typeof(Text));
            targetButton.OrderID = EditorGUILayout.IntField("Response Order ID", targetButton.OrderID);

            DrawDefaultInspector();
        }
    }
}

