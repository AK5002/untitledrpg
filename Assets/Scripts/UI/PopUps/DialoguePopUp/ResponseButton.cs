using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.DialoguePopUp
{
    public class ResponseButton : Button
    {
        [Header("ResponseButton Components")] 
         public RectTransform RectTransform; 
         public int OrderID;
         public Text ResponseText;
    }
}