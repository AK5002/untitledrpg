using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps.DialoguePopUp
{
    public class TradeConfirmationMenu : MonoBehaviour
    {
        [SerializeField] private Button m_YesButton;
        [SerializeField] private Button m_NoButton;

        public event Action<bool> OnDecisionMade;

        private void Awake()
        {
            m_YesButton.onClick.AddListener(() =>
            {
                OnDecisionMade?.Invoke(true);
                Destroy(gameObject);
            });
            m_NoButton.onClick.AddListener(() =>
            {
                OnDecisionMade?.Invoke(false);
                Destroy(gameObject);
            });
        }
    }
}