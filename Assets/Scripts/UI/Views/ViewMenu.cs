﻿using System;
using UnityEngine;

namespace UI.Views
{
    public abstract class ViewMenu : MonoBehaviour
    {
        protected ViewController ViewController { get; set; }


        public abstract Type Group { get; }


        public virtual void Setup(UIData data, ViewController viewController)
        {
            ViewController = viewController;
        }

        public abstract void OnShow();

        public virtual void OnHide()
        {
            Destroy(gameObject);
        }
    }
}