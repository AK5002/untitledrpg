﻿using System;
using Core.Game;
using QuestSystem;
using Services.DependencyInjection;
using UI.PopUps.InventoryMenu;
using UI.PopUps.QuestMenu;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class GamePlayView : ViewMenu
    {
        private GamePlayViewData Data;
        [SerializeField] private Text m_Health;
        [SerializeField] private Text m_Mana;
        [SerializeField] private Text m_Stamina;
        [SerializeField] private Button m_InventoryButton;
        [SerializeField] private Button m_PauseButton;
        [SerializeField] private Button m_QuestMenuButton;

        public override Type Group => typeof(GamePlayView);

        public override void Setup(UIData data, ViewController viewController)
        {
            base.Setup(data, viewController);
            Data = (GamePlayViewData) data;
        }

        public override void OnShow()
        {
            m_InventoryButton.onClick.RemoveAllListeners();
            m_PauseButton.onClick.RemoveAllListeners();
            m_QuestMenuButton.onClick.RemoveAllListeners();

            
            m_PauseButton.onClick.AddListener(ViewController.UiManager.OnPauseButton);
            m_InventoryButton.onClick.AddListener(() =>
            {
                var playerInventory = Injector.Instance.GetInstanceOf<Inventory>();
                var data = new InventoryPopUpData(playerInventory,null);
                ViewController.UiManager.PopUpController.Show<PlayerInventoryPopUp, InventoryPopUpData>(data);
            });
            m_QuestMenuButton.onClick.AddListener(() =>
            {
                var questController = Injector.Instance.GetInstanceOf<QuestController>();
                var data = new QuestPopUpData(questController.MainQuestData,questController.CurrentSideQuestData);
                ViewController.UiManager.PopUpController.Show<QuestPopUp, QuestPopUpData>(data);
            });
        }


        private void Update()
        {
            m_Health.text = $"{(int) Data.PlayerStats.Health}";
            m_Stamina.text = $"{(int) Data.PlayerStats.Stamina}";
            m_Mana.text = $"{(int) Data.PlayerStats.Mana}";
        }
    }
}